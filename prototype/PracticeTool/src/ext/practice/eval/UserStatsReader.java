package ext.practice.eval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import userstats.practice.eval.UserStats;

public class UserStatsReader {
	public static void readStats(UserStats userStats) {
		try {
			File f = new File(UserStatsWriter.USER_STATS_FILENAME);
			if (!f.canRead()) {
				FileWriter fw = new FileWriter(f);
				fw.write("0\n0\n0\n");
				fw.close();
			}
			
			FileReader fr = new FileReader(UserStatsWriter.USER_STATS_FILENAME);
			BufferedReader br = new BufferedReader(fr);
			String rankLine = br.readLine();
			String passLine = br.readLine();
			String failLine = br.readLine();
			userStats.setRank(convert(rankLine));
			userStats.setPasses(convert(passLine));
			userStats.setFailures(convert(failLine));
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.exit(1);
		}
	}
	
	private static int convert(String line) {
		if (line == null) return 0;
		
		try {
			return Integer.parseInt(line.trim());
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			System.exit(1);
		}
		return 0;
	}
}
