package ext.practice.eval;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import userstats.practice.eval.UserStats;

public class UserStatsWriter {
	public static final String USER_STATS_FILENAME = "./userstats.txt";
	
	public static void writeStats(UserStats userStats) {
		try {
			FileWriter fw = new FileWriter(USER_STATS_FILENAME);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("" + userStats.getRank() + "\n" + userStats.getPasses() + "\n" + userStats.getFailures() + "\n");
			bw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.exit(1);
		}
	}
}
