package midiplayer.practice.eval;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Track;

import midibuilder.practice.eval.MidiFile;

public class Player {
	private static Player instance = null;
	private Player() {}
	public static Player getPlayer() {
		if (instance == null) {
			instance = new Player();
		}
		return instance;
	}
	
	private Sequence getSequence(MidiFile file) {
		Sequence s = null;
		try {
			s = new Sequence(Sequence.PPQ, 200);
			Track t = s.createTrack();
			for (MidiEvent e : file.getEvents()) {
				t.add(e);
			}
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Sequence len: " + s.getMicrosecondLength());
		return s;
	}
	
	public void play(MidiFile file) {
        // Obtains the default Sequencer connected to a default device.
        Sequencer sequencer = null;
		try {
			sequencer = MidiSystem.getSequencer();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
			System.exit(1);
		}
 
        // Opens the device, indicating that it should now acquire any
        // system resources it requires and become operational.
        try {
			sequencer.open();
		} catch (MidiUnavailableException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
 
        Sequence seq = getSequence(file);
        

 
        // Sets the current sequence on which the sequencer operates.
        // The stream must point to MIDI file data.
        try {
            // create a stream from a file
            //InputStream is = new BufferedInputStream(new FileInputStream(new File("C:\\Users\\joelb\\Desktop\\BachBWV939.mid")));
     
            // Sets the current sequence on which the sequencer operates.
            // The stream must point to MIDI file data.
            //sequencer.setSequence(is);
            
			sequencer.setSequence(seq);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
			System.exit(1);
		}
 
        // Starts playback of the MIDI data in the currently loaded sequence.
        //System.out.println("Seq: " + seq);
        System.out.println("Starting playback");
        sequencer.start();
        while (sequencer.isRunning()) {System.out.print(".");}
        System.out.println("Ending playback");
	}
	
}
