package midiplayer.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import gen.practice.eval.GenerationException;
import gen.practice.eval.Generator;
import gen.practice.eval.Melody;
import midibuilder.practice.eval.MidiFile;
import userstats.practice.eval.OrigUserStats;

class PlayerTest {

	@Test
	void testGetPlayer() {
		Player p = Player.getPlayer();
		assert(p != null);
	}

	@Test
	void testPlay() {
		OrigUserStats stats = new OrigUserStats();
		stats.incrementBars();
		stats.incrementBars();
		stats.incrementBars();
		
		Melody m = null;
		for (int i = 0; i < 12; i++) stats.nextInterval();
		for (int i = 0; i < 5; i++) stats.nextRhythm();
		try {
			m = Generator.generate(null);
			System.out.println(m.toString());
		} catch (GenerationException e) {
			System.out.println(e.getMessage());
			fail();
		}
		
		MidiFile mf = m.toMidiFile();
		System.out.println("MidiFile: " + mf);
		Player.getPlayer().play(mf);
	}

}
