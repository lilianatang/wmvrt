package eval.practice.eval;

import gen.practice.eval.Bar;
import gen.practice.eval.Melody;
import gen.practice.eval.Note;
import gui.practice.eval.Director;

public class Evaluator {
	public static Evaluator _instance = null;
	public static final int EXTRACT_OFFSET = 10;
	
	private Evaluator() {
		
	}
	
	public static Evaluator get() {
		if (_instance == null) {
			_instance = new Evaluator();
		}
		return _instance;
	}
	
	public Evaluation performEvaluator(Melody melody, float[][] recordings) {
		Evaluation evaluation = new Evaluation();
		Bar[] mBars = melody.getBars();
		int melodyLen = mBars.length * Bar.TICKS_PER_BAR;
		float[][] extracts = createExtracts(recordings, melodyLen);
		int[] diffs = calculateDifferences(extracts, mBars);
		evaluation.setDiffs(diffs);
		return evaluation;
	}

	private int[] calculateDifferences(float[][] extract, Bar[] mBars) {
		int melodyLen = extract[0].length;
		int[] diffs = new int[melodyLen];
		for (int i = 0; i < melodyLen; i++) diffs[i] = 0;
		for (int extractIndex = 0; extractIndex < Director.NUMBER_OF_ANALYSES; extractIndex++) {
			for (int i = 0; i < melodyLen; i++) {
				Note recordedPitch = Note.getPitchName(extract[extractIndex][i]);
				Note melodyPitch = mBars[i / Bar.TICKS_PER_BAR].getTicks()[i % Bar.TICKS_PER_BAR].getNote();
				int recOrd = recordedPitch.ordinal();
				int mOrd = melodyPitch.ordinal();
				//diffs[i] = Math.max(recOrd, mOrd) - Math.min(recOrd, mOrd);
				diffs[i] += Math.min(Math.abs(recOrd - mOrd), Math.abs(mOrd - recOrd));
			}
		}
		for (int i = 0; i < melodyLen; i++) diffs[i] = Math.round((float) diffs[i] / (float) Director.NUMBER_OF_ANALYSES);
		return diffs;
	}

	private float[][] createExtracts(float[][] recording, int melodyLen) {
		float[][] extracts = new float[Director.NUMBER_OF_ANALYSES][melodyLen];
		for(int extractIndex = 0; extractIndex < Director.NUMBER_OF_ANALYSES; extractIndex++) {
			float current = (float) -1.0;
			for (int i = 0; i < EXTRACT_OFFSET; i++) {
				if (recording[extractIndex][i] > 0.0) {
					current = recording[extractIndex][i];
				}
			}
			for (int i = 0; i < melodyLen; i++) {
				extracts[extractIndex][i] = recording[extractIndex][EXTRACT_OFFSET + i];
				if (extracts[extractIndex][i] < 0.0) {
					extracts[extractIndex][i] = current;
				}
				else {
					current = extracts[extractIndex][i];
				}
			}
		}
		return extracts;
	}

}
