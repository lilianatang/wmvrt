package eval.practice.eval;

public class Evaluation {
	private String outputString = "";
	public int[] diffs = null;
	public float grade = Float.MAX_VALUE;
	
	public Evaluation() {
		outputString = "";
		grade = Float.MAX_VALUE;
	}
	
	public void setOutputString(String text) {
		outputString = text;
	}
	public String toString() {
		return outputString;
	}

	public int[] getDiffs() {
		return diffs;
	}

	public void setDiffs(int[] diffs) {
		this.diffs = diffs;
		calculateGrade();
	}
	
	private void calculateGrade() {
		float grade = (float) 0.0;
		double total = 0;
		for (int i = 0; i < diffs.length; i++) {
			total += (double) diffs[i];
		}
		grade = (float)total / (float) diffs.length;
		setGrade(grade);
	}

	public float getGrade() {
		return grade;
	}

	public void setGrade(float grade) {
		this.grade = grade;
		float lowest = Float.MAX_VALUE;
		int lowIndex = Integer.MAX_VALUE;
		
		StringBuffer b = new StringBuffer();

		b.append("");
		for (int i = 0; i < diffs.length; i++) {
			if (i > 0) b.append(' ');
			b.append(diffs[i]);
		}
		b.append('\n');
		b.append("Final Grade (lower is better): ");
		b.append(grade);
		b.append('\n');

		outputString = b.toString();
	}
	
	public boolean isPass() {
		return grade < 1.0;
	}

}
