package debugger.practice.eval;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class DebuggerMain extends JPanel {
	private JPanel buttonPanel = null;
	private JButton playButton = null;
	private JPanel oscPanel = null;
	private JPanel evalPanel = null;
	
	public static void launchDebuggerDialog() {
		JOptionPane.showMessageDialog(null, new DebuggerMain());
	}
	
	public DebuggerMain() {
		build();
	}
	
	private void build() {
		this.setLayout(new GridLayout(1,1));
		Box b = Box.createVerticalBox();
		b.add(getButtonPanel());
		b.add(getOscPanel());
		b.add(getEvalPanel());
		this.add(b);
	}

	private Component getEvalPanel() {
		if (evalPanel == null) {
			evalPanel = new JPanel();
			evalPanel.setLayout(new GridLayout(1,1));
			evalPanel.add(new DebuggerEvaluationPanel());
		}
		return evalPanel;
	}

	private Component getOscPanel() {
		if (oscPanel == null) {
			oscPanel = new JPanel();
			oscPanel.setLayout(new GridLayout(1,1));
			oscPanel.add(new JLabel("OSC"));
		}
		return oscPanel;
	}

	private Component getButtonPanel() {
		if (buttonPanel == null) {
			buttonPanel = new JPanel();
			buttonPanel.setLayout(new GridLayout(1,1));
			buttonPanel.add(getPlayButton());
		}
		return buttonPanel;
	}

	private Component getPlayButton() {
		if (playButton == null) {
			playButton = new JButton("Play");
			playButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					PlaybackThread p = new PlaybackThread();
					p.start();
				}
				
			});
		}
		return playButton;
	}
}
