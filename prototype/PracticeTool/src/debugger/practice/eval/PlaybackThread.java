package debugger.practice.eval;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import gui.practice.eval.Director;

public class PlaybackThread extends Thread {
	private final static int SAMPLING_RATE = 44100;

	public void run() {
		AudioFormat format;
		format = new AudioFormat(SAMPLING_RATE, 8, 1, true, true);
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
		try {
			SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(format);
			byte[] data = new byte[line.getBufferSize()];
			byte[] fullBuffer = Director.get().getRawBuffer();
			int totData = fullBuffer.length;
			int total = 0;
			line.start();
			while (total < totData) {
				for (int i = 0; i < data.length; i++) {
					if (total + i < totData) {
						data[i] = fullBuffer[total+i];
					}
				}
			    total += data.length; 
			    line.write(data, 0, data.length);
			}
			line.drain();
			line.stop();
			line.close();
			line = null;
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
