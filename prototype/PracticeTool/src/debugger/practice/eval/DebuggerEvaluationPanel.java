package debugger.practice.eval;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import gui.practice.eval.Director;

public class DebuggerEvaluationPanel extends JPanel {
	public DebuggerEvaluationPanel() {
		this.setLayout(new GridLayout(1,1));
		JTextArea t = new JTextArea(getText());
		t.setFont(new Font("monospaced", Font.PLAIN, 12));
		this.add(t);
	}

	private String getText() {
		StringBuffer buf = new StringBuffer();
		buf.append(Director.get().getMelody().toString());
		buf.append('\n');
		buf.append(Director.get().getEvaluation().toString());
		return buf.toString();
	}
}
