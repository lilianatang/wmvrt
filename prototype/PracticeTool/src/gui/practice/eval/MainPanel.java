package gui.practice.eval;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class MainPanel extends JPanel {
	public MainPanel() {
		this.setLayout(new BorderLayout());
		this.add(new StatePanel(), BorderLayout.NORTH);
		this.add(new EvaluationPanel(), BorderLayout.CENTER);
	}
}
