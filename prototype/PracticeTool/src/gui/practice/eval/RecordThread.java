package gui.practice.eval;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import be.tarsos.dsp.io.TarsosDSPAudioFloatConverter;
import be.tarsos.dsp.io.jvm.JVMAudioInputStream;
import be.tarsos.dsp.pitch.AMDF;
import be.tarsos.dsp.pitch.DynamicWavelet;
import be.tarsos.dsp.pitch.FFTPitch;
import be.tarsos.dsp.pitch.FastYin;
import be.tarsos.dsp.pitch.McLeodPitchMethod;
import be.tarsos.dsp.pitch.PitchDetector;
import be.tarsos.dsp.pitch.Yin;
import be.tarsos.dsp.test.TestUtilities;

public class RecordThread extends Thread {
	private final static int SAMPLING_RATE = 44100;
	private final static int MAX_LENGTH_IN_SAMPLES = 1000000;
	public RecordThread() {}
	private boolean keepGoing = true;
	private byte[] recordedData = null;
	
	public static float[] audioFile(byte[] bytes){
		float[] buffer = new float[bytes.length / 2]; //MAX_LENGTH_IN_SAMPLES];
		String file = "/wmVRT/evaluator/test/resources/demo.wav";
		final URL url = TestUtilities.class.getResource(file);
		try {
			AudioInputStream audioStream = AudioSystem.getAudioInputStream(url);
			AudioFormat format = audioStream.getFormat();
			TarsosDSPAudioFloatConverter converter = TarsosDSPAudioFloatConverter.getConverter(JVMAudioInputStream.toTarsosDSPFormat(format));
			//byte[] bytes = new byte[MAX_LENGTH_IN_SAMPLES * format.getSampleSizeInBits()];
			//audioStream.read(bytes);		
			converter.toFloatArray(bytes, buffer);
		} catch (IOException e) {
			throw new Error("Test audio file should be present.");
		} catch (UnsupportedAudioFileException e) {
			throw new Error("Test audio file format should be supported.");
		}	
		return buffer;
	}

	public static float[][] createSections(float[] buffer)
	{
		float[][] sections;
		float seconds = buffer.length / SAMPLING_RATE;
		System.out.println("Seconds: " + seconds);
		int nrSamples = (int) (seconds * 24);
		int sampleLen = (int) (SAMPLING_RATE / 24);
		sections = new float[nrSamples][sampleLen];
		int bufferCounter = 0;
		for (int i = 0; i < nrSamples; i++)
		{
			for (int j = 0; j < sampleLen; j++)
			{
				sections[i][j] = buffer[bufferCounter++];
			}
		}
		
		return sections;
	}
	
	public void run() {
		TargetDataLine line = null;
		AudioFormat format;
		format = new AudioFormat(SAMPLING_RATE, 8, 1, true, true);
		DataLine.Info info = new DataLine.Info(TargetDataLine.class,
		                format); // format is an AudioFormat object
		if (!AudioSystem.isLineSupported(info)) {
		    // Handle the error ...

		}
		// Obtain and open the line.
		try {
		    line = (TargetDataLine) AudioSystem.getLine(info);
		    line.open(format);
		} catch (LineUnavailableException ex) {
			ex.printStackTrace();
			System.exit(1);
		}

		// Assume that the TargetDataLine, line, has already
		// been obtained and opened.
		ByteArrayOutputStream out  = new ByteArrayOutputStream();
		int numBytesRead;
		byte[] data = new byte[line.getBufferSize()/5];
		byte[] fullBuffer = new byte[MAX_LENGTH_IN_SAMPLES];
		int fullBufferIndex = 0;

		StopThread stop = new StopThread(this);
		stop.start();
		// Begin audio capture.
		System.out.println("Recording...");
		line.start();

		// Here, stopped is a global boolean set by another thread.
		while (keepGoing) {
		    // Read the next chunk of data from the TargetDataLine.
		    numBytesRead =  line.read(data, 0, data.length);
		    // Save this chunk of data.
		    out.write(data, 0, numBytesRead);
		    int limit = fullBufferIndex + numBytesRead;
		    int dataIdx = 0;
		    while(fullBufferIndex < limit)
		    {
		    	fullBuffer[fullBufferIndex++] = data[dataIdx++];
		    }
		}
		
		line.close();
		
		recordedData = fullBuffer;
		System.out.println("Data recorded");
		Director.get().setRawBuffer(fullBuffer);
		
		// Load the audio data
		float[] buffer = audioFile(fullBuffer);
		System.out.println("Length: " + buffer.length);
		
		// Break the data up into samples, 24 samples per second
		float[][] sections = createSections(buffer);
		
		// Get the estimate for each sample
		int nrOfSections = sections.length;
		
		float mpm[] = new float[nrOfSections];
		float dWave[] = new float[nrOfSections];
		float fastYin[] = new float[nrOfSections];
		float amdf[] = new float[nrOfSections];
		float fftPitch[] = new float[nrOfSections];
		float yin[] = new float[nrOfSections];
		int bufferSize = (int) (SAMPLING_RATE / 24);
		
		PitchDetector dMpm = new McLeodPitchMethod(SAMPLING_RATE, bufferSize);
		PitchDetector dDWave = new DynamicWavelet(SAMPLING_RATE, bufferSize);
		PitchDetector dFastYin = new FastYin(SAMPLING_RATE, bufferSize);
		PitchDetector dAmdf = new AMDF(SAMPLING_RATE, bufferSize);
		PitchDetector dFftPitch = new FFTPitch(SAMPLING_RATE, bufferSize);
		PitchDetector dYin = new Yin(SAMPLING_RATE, bufferSize);
		
		for (int i = 0; i < nrOfSections; i++)
		{
			mpm[i] = dMpm.getPitch(sections[i]).getPitch();
			dWave[i] = dDWave.getPitch(sections[i]).getPitch();
			fastYin[i] = dFastYin.getPitch(sections[i]).getPitch();
			amdf[i] = dAmdf.getPitch(sections[i]).getPitch();
			fftPitch[i] = dFftPitch.getPitch(sections[i]).getPitch();
			yin[i] = dYin.getPitch(sections[i]).getPitch();
		}
		
		// Convert to a note value
		// Output the results
		for (int i = 0; i < nrOfSections; i++)
		{
			String output = "";
			output += i + ":\t";
			output += mpm[i] + "/" + PitchName.getPitchName(mpm[i]).getName() + "\t";
			output += dWave[i] + "/" + PitchName.getPitchName(dWave[i]).getName() + "\t";
			output += fastYin[i] + "/" + PitchName.getPitchName(fastYin[i]).getName() + "\t";
			output += amdf[i] + "/" + PitchName.getPitchName(amdf[i]).getName() + "\t";
			output += fftPitch[i] + "/" + PitchName.getPitchName(fftPitch[i]).getName() + "\t";
			output += yin[i] + "/" + PitchName.getPitchName(yin[i]).getName() + "\t";
			System.out.println(output);
		}
		
		Director.get().setNrOfSections(nrOfSections);
		Director.get().setMpm(mpm);
		Director.get().setFastYin(fastYin);
		Director.get().setAmdf(amdf);
		Director.get().setYin(yin);
		Director.get().triggerEvaluation();
		
	}
	
	public void stopGoing() {
		this.keepGoing = false;
	}
}
