package gui.practice.eval;

import debugger.practice.eval.DebuggerMain;
import eval.practice.eval.Evaluation;
import eval.practice.eval.Evaluator;
import gen.practice.eval.Melody;
import userstats.practice.eval.UserStats;

public class Director {
	public static final int MPM = 0;
	public static final int FAST_YIN = 1;
	public static final int AMDF = 2;
	public static final int YIN = 3;
	public static final int NUMBER_OF_ANALYSES = 4;
	
	private static Director _instance = null;
	private int nrOfSections = 0;
	private float[][] analyses = null;

	private Melody melody = null;
	private EvaluationPanel evaluationPanel = null;
	private boolean debug = true;
	private byte[] rawBuffer = null;
	private Evaluation evaluation = null;
	private UserStats userStats = null;
	

	private Director() {
		analyses = new float[NUMBER_OF_ANALYSES][];
		for (int i = 0; i < NUMBER_OF_ANALYSES; i++) analyses[i] = null;
		userStats = new UserStats();
	}
	public static Director get() {
		if (_instance == null)
		{
			_instance = new Director();
		}
		return _instance;
	}
	public int getNrOfSections() {
		return nrOfSections;
	}
	public void setNrOfSections(int nrOfSections) {
		this.nrOfSections = nrOfSections;
	}
	public float[] getMpm() {
		return analyses[MPM];
	}
	public void setMpm(float[] mpm) {
		this.analyses[MPM] = mpm;
	}
	public float[] getFastYin() {
		return analyses[FAST_YIN];
	}
	public void setFastYin(float[] fastYin) {
		this.analyses[FAST_YIN] = fastYin;
	}
	public float[] getAmdf() {
		return analyses[AMDF];
	}
	public void setAmdf(float[] amdf) {
		this.analyses[AMDF] = amdf;
	}
	public float[] getYin() {
		return analyses[YIN];
	}
	public void setYin(float[] yin) {
		this.analyses[YIN] = yin;
	}

	public Melody getMelody() {
		return melody;
	}
	public void setMelody(Melody melody) {
		this.melody = melody;
	}
	
	public EvaluationPanel getEvaluationPanel() {
		return evaluationPanel;
	}
	public void setEvaluationPanel(EvaluationPanel evaluationPanel) {
		this.evaluationPanel = evaluationPanel;
	}
	public void triggerEvaluation() {
		// Compare melody to recordings.
		evaluation = Evaluator.get().performEvaluator(getMelody(), analyses);
		if (evaluation.isPass()) {
			userStats.incrementPasses();
		} else {
			userStats.incrementFailures();
		}
		userStats.save();
		// Output the results to the evaluation panel.
		String text = userStats.toString() + "\n" + getRecordingInfoString() + getMelody().toString() + "\n" + evaluation.toString();
		getEvaluationPanel().updateText(text);
		if (debug) {
			launchDebuggerDialog();
		}
	}
	
	private void launchDebuggerDialog() {
		DebuggerMain.launchDebuggerDialog();
	}
	private String getRecordingInfoString() {
		// Convert to a note value
		// Output the results
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < nrOfSections; i++)
		{
			String output = "";
			output += i + ":\t";
			output += analyses[MPM][i] + "/" + PitchName.getPitchName(analyses[MPM][i]).getName() + "\t";
			output += analyses[FAST_YIN][i] + "/" + PitchName.getPitchName(analyses[FAST_YIN][i]).getName() + "\t";
			output += analyses[AMDF][i] + "/" + PitchName.getPitchName(analyses[AMDF][i]).getName() + "\t";
			output += analyses[YIN][i] + "/" + PitchName.getPitchName(analyses[YIN][i]).getName() + "\t";
			buf.append(output);
			buf.append("\n");
		}
		return buf.toString();
	}
	public byte[] getRawBuffer() {
		return rawBuffer;
	}
	public void setRawBuffer(byte[] rawBuffer) {
		this.rawBuffer = rawBuffer;
	}
	public Evaluation getEvaluation() {
		return evaluation;
	}
	public UserStats getUserStats() {
		return userStats;
	}
	public void setUserStats(UserStats userStats) {
		this.userStats = userStats;
	}
	
	
}
