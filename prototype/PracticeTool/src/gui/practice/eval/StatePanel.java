package gui.practice.eval;

import static org.junit.jupiter.api.Assertions.fail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import gen.practice.eval.GenerationException;
import gen.practice.eval.Generator;
import gen.practice.eval.Melody;
import midibuilder.practice.eval.MidiFile;
import midiplayer.practice.eval.Player;
import userstats.practice.eval.UserStats;

public class StatePanel extends JPanel {
	private JButton statusButton = null;
	
	public StatePanel() {
		this.add(getStatusButton());
	}
	
	public JButton getStatusButton() {
		if (statusButton == null) {
			statusButton = new JButton("Play");
			statusButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					UserStats stats = Director.get().getUserStats();
					
					Melody m = null;
					try {
						m = Generator.generate(stats);
						System.out.println(m.toString());
					} catch (GenerationException e) {
						System.out.println(e.getMessage());
						fail();
					}
					
					Director.get().setMelody(m);
					RecordThread rt = new RecordThread();
					MidiFile mf = m.toMidiFile();
					System.out.println("MidiFile: " + mf);
					Player.getPlayer().play(mf);
					rt.start();
				}
				
			});
		}
		return statusButton;
	}

}
