package gui.practice.eval;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class EvaluationPanel extends JPanel {
	private JTextArea textArea = null;
	public EvaluationPanel() {
		Director.get().setEvaluationPanel(this);
		JScrollPane scroll = new JScrollPane(getTextArea());
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.setLayout(new GridLayout(1,1));
		this.add(scroll);
		this.setSize(new Dimension(700, 800));
	}
	
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea("Evaluation");
			textArea.setSize(new Dimension(700, 800));
		}
		return textArea;
	}
	
	public void updateText(String text) {
		getTextArea().setText(text);
	}
}
