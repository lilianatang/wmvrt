package gui.practice.eval;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setContentPane(new MainPanel());
		//f.pack();
		f.setSize(new Dimension(700, 800));
		f.setVisible(true);
	}

}
