package gui.practice.eval;

public class StopThread extends Thread {
	private RecordThread recordThread = null;
	public StopThread(RecordThread r) {
		recordThread = r;
	}
	public void run() {
		try {
			sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		recordThread.stopGoing();
	}
}
