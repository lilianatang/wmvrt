package gui.practice.eval;

public enum PitchName {
	C,
	Cs,
	D,
	Ds,
	E,
	F,
	Fs,
	G,
	Gs,
	A,
	As,
	B;
	
	public static PitchName getPitchName(float freq)
	{
		if (freq < 1.0)
		{
			return C;
		}
		
		float val = freq;
		while (val < 247.0)
		{
			val = val * 2;
		}
		while (val > 523.0)
		{
			val = val / 2;
		}
		
		if (val < 270.0) return C;
		if (val < 285.0) return Cs;
		if (val < 306.0) return D;
		if (val < 320.0) return Ds;
		if (val < 339.0) return E;
		if (val < 359.0) return F;
		if (val < 380.0) return Fs;
		if (val < 404.0) return G;
		if (val < 432.0) return Gs;
		if (val < 453.0) return A;
		if (val < 485.0) return As;
		return B;
	}
	
	public String getName()
	{
		if (this == C) return "C ";
		if (this == Cs) return "C#";
		if (this == D) return "D ";
		if (this == Ds) return "D#";
		if (this == E) return "E ";
		if (this == F) return "F ";
		if (this == Fs) return "F#";
		if (this == G) return "G ";
		if (this == Gs) return "G#";
		if (this == A) return "A ";
		if (this == As) return "A#";
		return "B ";
	}
}
