package userstats.practice.eval;

import ext.practice.eval.UserStatsReader;
import ext.practice.eval.UserStatsWriter;

public class UserStats {
	public static final int MAX_BARS = 4;
	public static final int MIN_BARS = 2;
	public static final int RANK_UP_THRESHOLD = 8;
	
	private int rank = 0;
	private int passes = 0;
	private int failures = 0;
	
	public UserStats() {
		UserStatsReader.readStats(this);
	}

	public Interval getCurrentInterval() {
		switch(rank) {
		case 0: return Interval.TONIC;
		case 1: return Interval.MAJOR_SECOND;
		case 2: return Interval.MAJOR_SECOND;
		case 3: return Interval.MINOR_SECOND;
		case 4: return Interval.MAJOR_THIRD;
		case 5: return Interval.MAJOR_THIRD;
		case 6: return Interval.MINOR_THIRD;
		case 7: return Interval.PERFECT_FOURTH;
		case 8: return Interval.PERFECT_FIFTH;
		case 9: return Interval.PERFECT_FIFTH;
		case 10: return Interval.MAJOR_SIXTH;
		case 11: return Interval.MINOR_SIXTH;
		case 12: return Interval.MAJOR_SEVENTH;
		case 13: return Interval.MAJOR_SEVENTH;
		case 14: return Interval.MINOR_SEVENTH;
		case 15: return Interval.OCTAVE;
		case 16: return Interval.OCTAVE;
		case 17: return Interval.TRITONE;
		case 18: return Interval.MINOR_NINTH;
		case 19: return Interval.MAJOR_NINTH;
		}
		return Interval.ALL;
	}

	public Rhythm getCurrentRhythm() {
		switch(rank) {
		case 0: return Rhythm.WHOLE_NOTE;
		case 1: return Rhythm.WHOLE_NOTE;
		case 2: return Rhythm.HALF_NOTE;
		case 3: return Rhythm.HALF_NOTE;
		case 4: return Rhythm.HALF_NOTE;
		case 5: return Rhythm.QUARTER_NOTE;
		case 6: return Rhythm.QUARTER_NOTE;
		case 7: return Rhythm.QUARTER_NOTE;
		case 8: return Rhythm.QUARTER_NOTE;
		case 9: return Rhythm.EIGHTH_NOTE;
		case 10: return Rhythm.EIGHTH_NOTE;
		case 11: return Rhythm.EIGHTH_NOTE;
		case 12: return Rhythm.EIGHTH_NOTE;
		case 13: return Rhythm.HALF_TRIPLES;
		case 14: return Rhythm.HALF_TRIPLES;
		case 15: return Rhythm.HALF_TRIPLES;
		case 16: return Rhythm.QUARTER_TRIPLES;
		case 17: return Rhythm.QUARTER_TRIPLES;
		case 18: return Rhythm.QUARTER_TRIPLES;
		case 19: return Rhythm.QUARTER_TRIPLES;
		}
		return Rhythm.ALL;
	}

	public int getPasses() {
		return passes;
	}

	public int getFailures() {
		return failures;
	}

	public int getRank() {
		return rank;
	}
	
	public void setPasses(int passes) {
		this.passes = passes;
	}
	
	public void setFailures(int failures) {
		this.failures = failures;
	}
	
	public int getBars() {
		return MIN_BARS;
	}
	
	public void incrementPasses() {
		passes++;
		int failLog = (int) Math.round(Math.log((double)failures) / Math.log(2.0));
		if (passes - failLog > RANK_UP_THRESHOLD) {
			rank++;
			resetFields();
		}
	}
	
	public void incrementFailures() {
		failures++;
	}
	
	public void resetFields() {
		passes = 0;
		failures = 0;
	}
	
	// Only used for testing.
	public void setRank(int r) {
		this.rank = r;
		resetFields();
	}
	
	public void save() {
		UserStatsWriter.writeStats(this);
	}

	public String toString() {
		return "rank=" + rank + " passes=" + passes + " failures=" + failures;
	}
}
