package userstats.practice.eval;

public class OrigUserStats {
	public static final int MAX_BARS = 8;
	public static final int MIN_BARS = 2;
	
	private Interval currentInterval = Interval.TONIC;
	private Rhythm currentRhythm = Rhythm.WHOLE_NOTE;
	private int currentIntervalPasses = 0;
	private int currentIntervalFailures = 0;
	private int currentRhythmPasses = 0;
	private int currentRhythmFailures = 0;
	private int bars = 2;
	private int currentBarPasses = 0;
	private int currentBarFailures = 0;
	private VocalRange range = null;
	
	public OrigUserStats() {
		range = new VocalRange();
	}

	public Interval getCurrentInterval() {
		return currentInterval;
	}

	public Rhythm getCurrentRhythm() {
		return currentRhythm;
	}

	public int getCurrentIntervalPasses() {
		return currentIntervalPasses;
	}

	public int getCurrentIntervalFailures() {
		return currentIntervalFailures;
	}

	public int getCurrentRhythmPasses() {
		return currentRhythmPasses;
	}

	public int getCurrentRhythmFailures() {
		return currentRhythmFailures;
	}
	
	public int getBars() {
		return bars;
	}
	
	public int getCurrentBarPasses() {
		return currentBarPasses;
	}
	
	public int getCurrentBarFailures() {
		return currentBarFailures;
	}
	
	public void nextInterval() {
    	currentInterval = currentInterval.getNext();
	}
	
	public void nextRhythm() {
		currentRhythm = currentRhythm.getNext();
	}
	
	public void resetIntervalFields() {
		currentIntervalPasses = 0;
		currentIntervalFailures = 0;
	}
	
	public void incrementIntervalPasses() {
		currentIntervalPasses++;
	}
	
	public void incrementIntervalFailures() {
		currentIntervalFailures++;
	}
	
	public void resetRhythmFields() {
		currentRhythmPasses = 0;
		currentRhythmFailures = 0;
	}
	
	public void incrementRhythmPasses() {
		currentRhythmPasses++;
	}
	
	public void incrementRhythmFailures() {
		currentRhythmFailures++;
	}
	
	public void incrementBars() {
		if (bars < MAX_BARS) {
			bars++;
		}
	}
	
	public void incrementBarPasses() {
		currentBarPasses++;
	}
	
	public void incrementBarFailures() {
		currentBarFailures++;
	}
	
	public void resetBarFields() {
		currentBarPasses = 0;
		currentBarFailures = 0;
	}

	public VocalRange getCurrentRange() {
		// TODO Auto-generated method stub
		return null;
	}
}
