package userstats.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OrigUserStatsTest {

	@Test
	void testGetCurrentInterval() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentInterval() == Interval.TONIC);
		for (Interval i : Interval.values()) {
			assert(u.getCurrentInterval() == i);
			u.nextInterval();
		}
	}

	@Test
	void testGetCurrentRhythm() {
		OrigUserStats u = new OrigUserStats();
		for (Rhythm r : Rhythm.values()) {
			assert(u.getCurrentRhythm() == r);
			u.nextRhythm();
		}
	}

	@Test
	void testGetCurrentIntervalPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentIntervalPasses() == 0);
		u.incrementIntervalPasses();
		u.incrementIntervalPasses();
		assert(u.getCurrentIntervalPasses() == 2);
	}

	@Test
	void testGetCurrentIntervalFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentIntervalFailures() == 0);
		u.incrementIntervalFailures();
		u.incrementIntervalFailures();
		assert(u.getCurrentIntervalFailures() == 2);
	}

	@Test
	void testGetCurrentRhythmPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRhythmPasses() == 0);
		u.incrementRhythmPasses();
		u.incrementRhythmPasses();
		assert(u.getCurrentRhythmPasses() == 2);
	}

	@Test
	void testGetCurrentRhythmFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRhythmFailures() == 0);
		u.incrementRhythmFailures();
		u.incrementRhythmFailures();
		assert(u.getCurrentRhythmFailures() == 2);
	}

	@Test
	void testGetBars() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getBars() == 2);
		u.incrementBars();
		assert(u.getBars() == 3);
		for (int i = 4; i <= OrigUserStats.MAX_BARS + 3; i++) {
			u.incrementBars();
		}
		assert(u.getBars() == OrigUserStats.MAX_BARS);
	}

	@Test
	void testGetCurrentBarPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarPasses() == 0);
		u.incrementBarPasses();
		u.incrementBarPasses();
		assert(u.getCurrentBarPasses() == 2);
	}

	@Test
	void testGetCurrentBarFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarFailures() == 0);
		u.incrementBarFailures();
		u.incrementBarFailures();
		assert(u.getCurrentBarFailures() == 2);
	}

	@Test
	void testNextInterval() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentInterval() == Interval.TONIC);
		for (Interval i : Interval.values()) {
			assert(u.getCurrentInterval() == i);
			u.nextInterval();
		}
		u.nextInterval();
		u.nextInterval();
		assert(u.getCurrentInterval() == Interval.ALL);
	}

	@Test
	void testNextRhythm() {
		OrigUserStats u = new OrigUserStats();
		for (Rhythm r : Rhythm.values()) {
			assert(u.getCurrentRhythm() == r);
			u.nextRhythm();
		}
		u.nextRhythm();
		u.nextRhythm();
		assert(u.getCurrentRhythm() == Rhythm.ALL);
	}

	@Test
	void testResetIntervalFields() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentIntervalFailures() == 0);
		assert(u.getCurrentIntervalPasses() == 0);
		u.incrementIntervalFailures();
		u.incrementIntervalPasses();
		assert(u.getCurrentIntervalFailures() > 0);
		assert(u.getCurrentIntervalPasses() > 0);
		u.resetIntervalFields();
		assert(u.getCurrentIntervalFailures() == 0);
		assert(u.getCurrentIntervalPasses() == 0);
	}

	@Test
	void testIncrementIntervalPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentIntervalPasses() == 0);
		for (int i = 0; i < 10; i++) {
			assert(u.getCurrentIntervalPasses() == i);
			u.incrementIntervalPasses();
		}
	}

	@Test
	void testIncrementIntervalFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarFailures() == 0);
		for (int i = 0; i < 10; i++) {
			assert(u.getCurrentIntervalFailures() == i);
			u.incrementIntervalFailures();
		}
	}

	@Test
	void testResetRhythmFields() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRhythmFailures() == 0);
		assert(u.getCurrentRhythmPasses() == 0);
		u.incrementRhythmFailures();
		u.incrementRhythmPasses();
		assert(u.getCurrentRhythmFailures() > 0);
		assert(u.getCurrentRhythmPasses() > 0);
		u.resetRhythmFields();
		assert(u.getCurrentRhythmFailures() == 0);
		assert(u.getCurrentRhythmPasses() == 0);
	}

	@Test
	void testIncrementRhythmPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRhythmPasses() == 0);
		u.incrementRhythmPasses();
		u.incrementRhythmPasses();
		assert(u.getCurrentRhythmPasses() == 2);
	}

	@Test
	void testIncrementRhythmFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRhythmFailures() == 0);
		u.incrementRhythmFailures();
		u.incrementRhythmFailures();
		assert(u.getCurrentRhythmFailures() == 2);
	}

	@Test
	void testIncrementBars() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getBars() == OrigUserStats.MIN_BARS);
		for (int i = OrigUserStats.MIN_BARS; i < OrigUserStats.MAX_BARS; i++) {
			assert(u.getBars() == i);
			u.incrementBars();
		}
		u.incrementBars();
		u.incrementBars();
		assert(u.getBars() == OrigUserStats.MAX_BARS);
	}

	@Test
	void testIncrementBarPasses() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarPasses() == 0);
		u.incrementBarPasses();
		u.incrementBarPasses();
		assert(u.getCurrentBarPasses() == 2);
	}

	@Test
	void testIncrementBarFailures() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarFailures() == 0);
		u.incrementBarFailures();
		u.incrementBarFailures();
		assert(u.getCurrentBarFailures() == 2);
	}

	@Test
	void testResetBarFields() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentBarFailures() == 0);
		assert(u.getCurrentBarPasses() == 0);
		u.incrementBarFailures();
		u.incrementBarPasses();
		assert(u.getCurrentBarFailures() > 0);
		assert(u.getCurrentBarPasses() > 0);
		u.resetBarFields();
		assert(u.getCurrentBarFailures() == 0);
		assert(u.getCurrentBarPasses() == 0);
	}

	@Test
	void testGetCurrentRange() {
		OrigUserStats u = new OrigUserStats();
		assert(u.getCurrentRange() == null);
	}

}
