package userstats.practice.eval;

public enum Rhythm {
	WHOLE_NOTE,
	HALF_NOTE,
	QUARTER_NOTE,
	EIGHTH_NOTE,
	HALF_TRIPLES,
	QUARTER_TRIPLES,
	ALL;
	
	public Rhythm getNext() {
		if (this == ALL) return ALL;
		int currentVal = this.ordinal();
		currentVal++;
		return Rhythm.values()[currentVal];
	}
}
