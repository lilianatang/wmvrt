package midibuilder.practice.eval;

import java.util.Vector;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;

import gen.practice.eval.Note;

public class MidiFile {
	public static final int VELOCITY = 80;
	private MidiHeader headerChunk = null;
	private MidiTrack track = null;
	
	public MidiFile()
	{
		headerChunk = new MidiHeader();
		track = new MidiTrack();
	}
	
	public void noteOn(Note note, int octave, long time) {
		int noteValue = (octave + 1) * 12;
		noteValue += note.ordinal();
		try {
			ShortMessage s = new ShortMessage(ShortMessage.NOTE_ON, 0, noteValue, VELOCITY);
			MidiEvent e = new MidiEvent(s, time);
			track.addEvent(e);
		} catch (InvalidMidiDataException imde) {
			System.out.println(imde);
			imde.printStackTrace();
			System.exit(1);
		}
	}

	public void noteOff(Note note, int octave, long time) {
		int noteValue = (octave + 1) * 12;
		noteValue += note.ordinal();
		try {
			ShortMessage s = new ShortMessage(ShortMessage.NOTE_OFF, 0, noteValue, VELOCITY);
			MidiEvent e = new MidiEvent(s, time);
			track.addEvent(e);
		} catch (InvalidMidiDataException imde) {
			System.out.println(imde);
			imde.printStackTrace();
			System.exit(1);
		}
	}
	
	public Vector<MidiEvent> getEvents() {
		return track.getEvents();
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (MidiEvent e : getEvents()) {
			buf.append(e);
			buf.append(' ');
		}
		return buf.toString();
	}
}
