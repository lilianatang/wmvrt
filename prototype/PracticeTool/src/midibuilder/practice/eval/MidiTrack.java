package midibuilder.practice.eval;

import java.util.Vector;

import javax.sound.midi.MidiEvent;

public class MidiTrack {
	private Vector<MidiEvent> events = null;
	
	public MidiTrack() {
		events = new Vector<MidiEvent>();
	}
	
	public void addEvent(MidiEvent e) {
		events.add(e);
	}
	
	public Vector<MidiEvent> getEvents() {
		return events;
	}
}
