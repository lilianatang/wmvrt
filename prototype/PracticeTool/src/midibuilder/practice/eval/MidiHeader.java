package midibuilder.practice.eval;

public class MidiHeader {
	private String chunkType = "MThd";
	private int len = 6;
	private int format = 0;
	private int ntrks = 1;
	private int division = 48;
	public String getChunkType() {
		return chunkType;
	}
	public int getLen() {
		return len;
	}
	public int getFormat() {
		return format;
	}
	public int getNtrks() {
		return ntrks;
	}
	public int getDivision() {
		return division;
	}
	public void setDivision(int division) {
		this.division = division;
	}
	
	public String toString() {
		String s = chunkType + "{len=" + len + " format=" + format + " ntrks=" + ntrks + " division=" + division + "}";
		return s;
	}
}
