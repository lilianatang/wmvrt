package gen.practice.eval;

import midibuilder.practice.eval.MidiFile;

public class Melody {
	public static final int TICK_TIME_FACTOR = 40;
	private Bar bars[] = null;
	
	public Melody(int numberOfBars) {
		bars = new Bar[numberOfBars];
		for (int i = 0; i < numberOfBars; i++) {
			bars[i] = new Bar();
		}
	}
	
	public Bar[] getBars() {
		return bars;
	}
	
	public String toString() {
		String s = "";
		for (Bar b : bars) {
			s += b.toString();
		}
		return s;
	}
	
	public String OLDtoString() {
		String s = "|";
		for (Bar b : bars) {
			s += b.toString() + "|";
		}
		return s;
	}
	
	public MidiFile toMidiFile() {
		MidiFile f = new MidiFile();
		
		f.noteOn(Note.C, 0, 0 * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, 1 * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, 6 * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, 7 * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, 12 * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, 13 * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, 18 * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, 19 * TICK_TIME_FACTOR);
		
		int tickCount = 24;
		
		for (Bar b : bars) {
			for (Tick t : b.getTicks()) {
				// Add tick information to midi file.
				if (t.isStart()) {
					f.noteOn(t.getNote(), t.getOctave(), tickCount * TICK_TIME_FACTOR);
				} else if (t.isEnd()) {
					f.noteOff(t.getNote(), t.getOctave(), tickCount * TICK_TIME_FACTOR);
				}
				tickCount++;
			}
		}
		
		f.noteOn(Note.C, 0, (tickCount + 0) * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, (tickCount + 1) * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, (tickCount + 6) * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, (tickCount + 7) * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, (tickCount + 12) * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, (tickCount + 13) * TICK_TIME_FACTOR);
		f.noteOn(Note.C, 0, (tickCount + 18) * TICK_TIME_FACTOR);
		f.noteOff(Note.C, 0, (tickCount + 19) * TICK_TIME_FACTOR);
		
		return f;
	}
}
