package gen.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MelodyTest {

	@Test
	void testMelody() {
		Melody m = new Melody(4);
		assert(m != null);
		assert(m.getBars().length == 4);
	}

	@Test
	void testGetBars() {
		Melody m = new Melody(2);
		assert(m.getBars().length == 2);
	}

}
