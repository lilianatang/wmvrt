package gen.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BarTest {

	@Test
	void testBar() {
		Bar b = new Bar();
		assert(b != null);
	}

	@Test
	void testGetTicks() {
		Bar b = new Bar();
		Tick[] t = b.getTicks();
		assert(t.length == Bar.TICKS_PER_BAR);
	}

}
