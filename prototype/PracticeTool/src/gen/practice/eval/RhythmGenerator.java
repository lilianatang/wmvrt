package gen.practice.eval;

import java.util.Vector;

import userstats.practice.eval.Rhythm;

class RhythmDetails {
	private Rhythm rhythm = null;
	private int ticks = 0;
	
	public RhythmDetails(Rhythm rhythm, int ticks) {
		this.rhythm = rhythm;
		this.ticks = ticks;
	}
	
	public boolean enoughRoom(int ticks) {
		return this.ticks <= ticks;
	}
	
	public Rhythm getRhythm() {
		return rhythm;
	}
	
	public int setupRhythm(Bar bar, int start) throws GenerationException {
		System.out.println("" + this.rhythm + "/" + start + "/" + this.ticks);
		Tick[] ticks = bar.getTicks();
		switch (rhythm) {
		case WHOLE_NOTE:
		case HALF_NOTE:
		case QUARTER_NOTE:
		case EIGHTH_NOTE:
			ticks[start].setStart(true);
			ticks[start + this.ticks - 1].setEnd(true);
			break;
		case HALF_TRIPLES:
		case QUARTER_TRIPLES:
			int duration = this.ticks / 3;
			for (int i = 0; i < 3; i++) {
				ticks[start + (i*duration)].setStart(true);
				ticks[start + (i*duration) + duration - 1].setEnd(true);
			}
			break;
		default:
			throw new GenerationException("Not enough room for rhythm in bar.");
		}
		return this.ticks;
	}
}

public class RhythmGenerator {
	public static RhythmDetails[] rhythms = {
			new RhythmDetails(Rhythm.WHOLE_NOTE, Bar.TICKS_PER_BAR),
			new RhythmDetails(Rhythm.HALF_NOTE, Bar.TICKS_PER_BAR/2),
			new RhythmDetails(Rhythm.HALF_TRIPLES, Bar.TICKS_PER_BAR),
			new RhythmDetails(Rhythm.QUARTER_NOTE, Bar.TICKS_PER_BAR/4),
			new RhythmDetails(Rhythm.QUARTER_TRIPLES, Bar.TICKS_PER_BAR/2),
			new RhythmDetails(Rhythm.EIGHTH_NOTE, Bar.TICKS_PER_BAR/8)
	};

	public static void generate(Melody melody, Rhythm rhythm) throws GenerationException {
		for (Bar bar : melody.getBars()) {
			int currentLocation = 0;
			while (currentLocation < Bar.TICKS_PER_BAR) {
				// Get all rhythms that fit
				Vector<RhythmDetails> rhythmsThatFit = new Vector<RhythmDetails>();
				int remaining = Bar.TICKS_PER_BAR - currentLocation;
				for (RhythmDetails r : rhythms) {
					if (r.enoughRoom(remaining) && r.getRhythm().ordinal() <= rhythm.ordinal()) {
						rhythmsThatFit.add(r);
					}
				}
				
				// Randomly choose a rhythm
				int choice = (int) (Math.random() * rhythmsThatFit.size());
				
				// Add rhythm
				currentLocation += rhythmsThatFit.elementAt(choice).setupRhythm(bar, currentLocation);
			}
		}
	}

}
