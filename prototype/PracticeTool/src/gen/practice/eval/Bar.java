package gen.practice.eval;

public class Bar {
	public static final int TICKS_PER_BAR = 24;
	
	private Tick ticks[] = null;
	
	public Bar() {
		ticks = new Tick[TICKS_PER_BAR];
		for (int i = 0; i < TICKS_PER_BAR; i++) {
			ticks[i] = new Tick();
		}
	}
	
	public Tick[] getTicks() {
		return ticks;
	}
	
	public String toString() {
		String s = "";
		for (Tick t : ticks) {
			s = s + t.toString();
		}
		return s;
	}
}
