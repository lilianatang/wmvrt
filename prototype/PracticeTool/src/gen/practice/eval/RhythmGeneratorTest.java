package gen.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import userstats.practice.eval.Rhythm;

class RhythmGeneratorTest {

	@Test
	void testGenerator() {
		Melody m = new Melody(2);
		try {
			RhythmGenerator.generate(m, Rhythm.WHOLE_NOTE);
			System.out.println(m.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		m = new Melody(8);
		try {
			RhythmGenerator.generate(m, Rhythm.ALL);
			System.out.println(m.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
