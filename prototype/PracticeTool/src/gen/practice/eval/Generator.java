package gen.practice.eval;

import userstats.practice.eval.Interval;
import userstats.practice.eval.Rhythm;
import userstats.practice.eval.UserStats;
import userstats.practice.eval.VocalRange;

public class Generator {
	private Generator() {}
	
	public static Melody generate(UserStats stats) throws GenerationException {
		Melody melody = new Melody(stats.getBars());
		setupRhythm(melody, stats.getCurrentRhythm());
		setupNotes(melody, stats.getCurrentInterval());
		return melody;
	}

	private static void setupRhythm(Melody melody, Rhythm rhythm) throws GenerationException {
		RhythmGenerator.generate(melody, rhythm);
	}
	private static void setupNotes(Melody melody, Interval interval) {
		MelodyGenerator.generate(melody, interval);
	}

}
