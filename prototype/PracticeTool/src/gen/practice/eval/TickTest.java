package gen.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TickTest {

	@Test
	void testTick() {
		Tick t = new Tick();
		assert(t.getNote() == null);
		assert(t.getOctave() == 3);
		assert(t.isStart() == false);
		assert(t.isEnd() == false);
	}

	@Test
	void testIsStart() {
		Tick t = new Tick();
		assert(t.isStart() == false);
		t.setStart(true);
		assert(t.isStart() == true);
	}

	@Test
	void testSetStart() {
		Tick t = new Tick();
		t.setStart(true);
		assert(t.isStart() == true);
		t.setStart(false);
		assert(t.isStart() == false);
	}

	@Test
	void testIsEnd() {
		Tick t = new Tick();
		assert(t.isEnd() == false);
		t.setEnd(true);
		assert(t.isEnd() == true);
	}

	@Test
	void testSetEnd() {
		Tick t = new Tick();
		t.setEnd(true);
		assert(t.isEnd() == true);
		t.setEnd(false);
		assert(t.isEnd() == false);
	}

	@Test
	void testGetNote() {
		Tick t = new Tick();
		assert(t.getNote() == null);
		t.setNote(Note.C);
		assert(t.getNote() == Note.C);
	}

	@Test
	void testSetNote() {
		Tick t = new Tick();
		for (Note n : Note.values()) {
			t.setNote(n);
			assert(t.getNote() == n);
		}
	}

	@Test
	void testGetOctave() {
		Tick t = new Tick();
		assert(t.getOctave() == 3);
	}

	@Test
	void testSetOctave() {
		Tick t = new Tick();
		t.setOctave(Tick.MIN_OCTAVE);;
		assert(t.getOctave() == Tick.MIN_OCTAVE);
		t.setOctave(Tick.MIN_OCTAVE - 1);
		assert(t.getOctave() == Tick.MIN_OCTAVE);
		t.setOctave(Tick.MAX_OCTAVE);
		assert(t.getOctave() == Tick.MAX_OCTAVE);
		t.setOctave(Tick.MAX_OCTAVE + 1);
		assert(t.getOctave() == Tick.MAX_OCTAVE);
	}

}
