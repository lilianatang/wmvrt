package gen.practice.eval;

import userstats.practice.eval.Interval;
import userstats.practice.eval.VocalRange;

public class MelodyGenerator {

	public static void generate(Melody melody, Interval interval) {
		Note currentNote = Note.C;
		int currentOctave = 3;
		int relativeInterval = 0;
		for (Bar bar : melody.getBars()) {
			for (Tick tick : bar.getTicks()) {
				if (tick.isStart()) {
					relativeInterval = addRandomNote(tick, currentNote, currentOctave, interval, relativeInterval);
					currentNote = tick.getNote();
					currentOctave = tick.getOctave();
				} else {
					tick.setNote(currentNote);
					tick.setOctave(currentOctave);
				}
			}
		}
		
	}

	private static int addRandomNote(Tick tick, Note currentNote, int currentOctave, Interval interval, int relativeInterval) {
		int randomVal = (int)(Math.random() * interval.ordinal());
		Interval randomInterval = Interval.values()[randomVal];
		int direction = 1;
		int randomDirection = (int)(Math.random() * 2);
		if (randomDirection == 0) direction = -1;
		
		int noteInt = currentNote.ordinal();
		int intervalInt = randomInterval.asInt() * direction;
		
		if (Math.abs(relativeInterval + intervalInt) > 16) {
			intervalInt = intervalInt * -1;
		}
		
		noteInt += intervalInt;
		currentOctave += (int) (noteInt / 12);
		noteInt = Math.abs(noteInt % 12);
		tick.setNote(Note.values()[noteInt]);
		tick.setOctave(currentOctave);
		return relativeInterval + intervalInt;
	}

}
;