package gen.practice.eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import userstats.practice.eval.UserStats;

class GeneratorTest {

	@Test
	void testGenerate() {
		UserStats stats = new UserStats();
		stats.setRank(2);
		
		try {
			Melody m = Generator.generate(stats);
			System.out.println(m.toString());
		} catch (GenerationException e) {
			System.out.println(e.getMessage());
			fail();
		}
	}

}
