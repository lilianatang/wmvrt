package gen.practice.eval;

public class GenerationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6662169406550518044L;

	public GenerationException(String msg) {
		super(msg);
	}
}
