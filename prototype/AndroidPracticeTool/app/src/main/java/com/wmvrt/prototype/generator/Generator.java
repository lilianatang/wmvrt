package com.wmvrt.prototype.generator;


import com.wmvrt.prototype.userstats.UserStats;

/**
 * The generator creates a melody appropriate for a user based
 * on the user's stats.
 */
public class Generator {
    private Generator() {}

    /**
     * Create the melody. First set up the rhythm and then fill in the notes.
     * @param stats
     * @return
     * @throws GenerationException
     */
    public static Melody generate(UserStats stats) throws GenerationException {
        Melody melody = new Melody(stats.getBars());
        setupRhythm(melody, stats.getCurrentRhythm());
        setupNotes(melody, stats.getCurrentInterval());
        return melody;
    }

    /**
     * Sets up the rhythm of the melody using the RhythmGenerator.
     * @param melody
     * @param rhythm
     * @throws GenerationException
     */
    private static void setupRhythm(Melody melody, Rhythm rhythm) throws GenerationException {
        RhythmGenerator.generate(melody, rhythm);
    }

    /**
     * Sets up the pitches in the melody using the MelodyGenerator.
     * @param melody
     * @param interval
     */
    private static void setupNotes(Melody melody, Interval interval) {
        MelodyGenerator.generate(melody, interval);
    }

}

