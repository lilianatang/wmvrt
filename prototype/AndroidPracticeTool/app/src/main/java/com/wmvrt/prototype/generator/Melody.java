package com.wmvrt.prototype.generator;

import java.util.Vector;

/**
 * A melody is a sequence of notes. We break the melody into bars and then into ticks and
 * assign a pitch to each tick.
 */
public class Melody {
    public static final int TICK_TIME_FACTOR = 40;
    private Bar bars[] = null;

    /**
     * Sets up an empty melody that is a particular length.
     * @param numberOfBars
     */
    public Melody(int numberOfBars) {
        bars = new Bar[numberOfBars];
        for (int i = 0; i < numberOfBars; i++) {
            bars[i] = new Bar();
        }
    }

    /**
     * Gets the melody's bars.
     * @return
     */
    public Bar[] getBars() {
        return bars;
    }

    /**
     * Returns a string representation of the melody.
     * @return
     */
    public String toString() {
        String s = "";
        for (Bar b : bars) {
            s += b.toString();
        }
        return s;
    }

    /**
     * A thump is an extremely low note that sounds like a 'thump'.
     * We use thumps to give the user the beat before the melody.
     * @param s
     * @param tickTime
     */
    private void addThumps(Vector<SequenceItem> s, int tickTime) {
        for (int i = 0; i < 4; i++) {
            s.add(new SequenceItem(Note.C.getFrequency(1), tickTime));
            s.add(new SequenceItem(0.0, tickTime * 5));
        }
    }

    /**
     * Returns the melody as a sequence of notes. These notes are in a form that is
     * easily applied to the AudioEngine. Part of this functions responsibility
     * is converting ticks into actual times. The tick time is defined as a tenth
     * of the sampling rate.
     * @param sampleRate
     * @return
     */
    public Vector<SequenceItem> toSequence(int sampleRate) {
        int tickTime = sampleRate / 10;
        Vector<SequenceItem> s = new Vector<SequenceItem>();
        s.add(new SequenceItem(0,tickTime * 6));
        this.addThumps(s, tickTime);

        double freq = 0.0;
        int tickCount = 0;
        for (Bar b : bars) {
            for (Tick t : b.getTicks()) {
                if (t.isStart()) {
                    freq = t.getNote().getFrequency(t.getOctave());
                } else if (t.isEnd()) {
                    tickCount++;
                    s.add(new SequenceItem(freq, tickTime * tickCount));
                    tickCount = 0;
                } else {
                    tickCount++;
                }
            }
        }

        this.addThumps(s, tickTime);
        return s;
    }

    /**
     * Calculates the duration of a melody in terms of milliseconds. We need this for our
     * timers that are triggered at the end of playing a melody and also to start and end
     * recording the performance.
     * @param seq
     * @param sampleRate
     * @return
     */
    public static int durationInMilliseconds(Vector<SequenceItem> seq, int sampleRate) {
        long seqTime = 0;
        for (SequenceItem s : seq) {
            seqTime += (long) s.time;
        }
        //seqTime *= 10; // tick factor
        seqTime *= 1000; // to milliseconds
        seqTime /= sampleRate;
        return (int) seqTime;
    }

}

