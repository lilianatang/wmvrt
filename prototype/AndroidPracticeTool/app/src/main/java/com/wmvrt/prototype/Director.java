package com.wmvrt.prototype;

import com.wmvrt.prototype.evaluator.AudioAnalysis;
import com.wmvrt.prototype.evaluator.Evaluation;
import com.wmvrt.prototype.evaluator.Evaluator;
import com.wmvrt.prototype.generator.GenerationException;
import com.wmvrt.prototype.generator.Generator;
import com.wmvrt.prototype.generator.Melody;
import com.wmvrt.prototype.generator.SequenceItem;
import com.wmvrt.prototype.userstats.UserStats;

import java.util.Vector;

public class Director {
    private static Director __instance = null;
    private UserStats userStats;
    private MainActivity mainActivity = null;
    private Melody melody = null;
    private int nrOfSections = 0;
    public static final int MPM = 0;
    public static final int FAST_YIN = 1;
    public static final int AMDF = 2;
    public static final int YIN = 3;
    public static final int NUMBER_OF_ANALYSES = 4;
    private float[][] analyses = null;
    private Evaluation evaluation = null;


    private Director() {
        analyses = new float[NUMBER_OF_ANALYSES][];
        for (int i = 0; i < NUMBER_OF_ANALYSES; i++) analyses[i] = null;
        userStats = new UserStats();
//        for (int i = 0; i < 60; i++) {
//            userStats.incrementPasses();
//        }
    }

    public static Director getDirector() {
        if (__instance == null) {
            __instance = new Director();
        }
        return __instance;
    }

    public Vector<SequenceItem> generateSequence(int sampleRate) {
        melody = null;
        try {
            melody = Generator.generate(userStats);
        } catch(GenerationException ge) {
            ge.printStackTrace();
        }
        return melody.toSequence(sampleRate);
    }

    public void setMainActivity(MainActivity activity) {
        this.mainActivity = activity;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void processRecording(byte[] buffer, int sampleRate) {
        System.out.println("BUFFER Has " + buffer.length + " bytes to process");
        AudioAnalysis.detect(buffer, sampleRate);
    }

    public void setNrOfSections(int nrOfSections) {
        this.nrOfSections = nrOfSections;
    }
    public int getNrOfSections() {
        return nrOfSections;
    }

    public float[] getMpm() {
        return analyses[MPM];
    }
    public void setMpm(float[] mpm) {
        this.analyses[MPM] = mpm;
    }
    public float[] getFastYin() {
        return analyses[FAST_YIN];
    }
    public void setFastYin(float[] fastYin) {
        this.analyses[FAST_YIN] = fastYin;
    }
    public float[] getAmdf() {
        return analyses[AMDF];
    }
    public void setAmdf(float[] amdf) {
        this.analyses[AMDF] = amdf;
    }
    public float[] getYin() {
        return analyses[YIN];
    }
    public void setYin(float[] yin) {
        this.analyses[YIN] = yin;
    }

    public void triggerEvaluation() {
        System.out.println("TRIGGER EVALUATION");
        // Compare melody to recordings.
        evaluation = Evaluator.get().performEvaluator(melody, analyses);
        if (evaluation.isPass()) {
            userStats.incrementPasses();
        } else {
            userStats.incrementFailures();
        }
        userStats.save();
        // Output the results to the evaluation panel.
//		String text = userStats.toString() + "\n" + getRecordingInfoString() + getMelody().toString() + "\n" + evaluation.toString();
        String text = userStats.toString() + "\n" + melody.toString() + "\n" + evaluation.toString();
        System.out.println(text);
        setText(text);
//        getEvaluationPanel().updateText(text);
//        if (debug) {
//            launchDebuggerDialog();
//        }
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setText(String text) {
        if (mainActivity != null) {
            mainActivity.setText(text);
        }
    }
}
