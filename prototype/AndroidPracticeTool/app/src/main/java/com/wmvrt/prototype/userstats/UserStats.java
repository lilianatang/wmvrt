package com.wmvrt.prototype.userstats;


import com.wmvrt.prototype.generator.Interval;
import com.wmvrt.prototype.generator.Rhythm;

/**
 * The UserStats class holds the fields that keep track of a
 * user's progress through the practice app. They include
 * methods to allow saving and loading user stats data.
 */
public class UserStats {
    public static final int MAX_BARS = 4;
    public static final int MIN_BARS = 2;
    public static final int RANK_UP_THRESHOLD = 8;

    private int rank = 0;
    private int passes = 0;
    private int failures = 0;

    /**
     * Constructor. Reads the current user stats from the device.
     */
    public UserStats() {
        UserStatsReader.readStats(this);
    }

    /**
     * Gets the user's progress on intervals based on
     * the user's rank. This is used in generating
     * appropriate melodies.
     * @return
     */
    public Interval getCurrentInterval() {
        switch(rank) {
            case 0: return Interval.TONIC;
            case 1: return Interval.MAJOR_SECOND;
            case 2: return Interval.MAJOR_SECOND;
            case 3: return Interval.MINOR_SECOND;
            case 4: return Interval.MAJOR_THIRD;
            case 5: return Interval.MAJOR_THIRD;
            case 6: return Interval.MINOR_THIRD;
            case 7: return Interval.PERFECT_FOURTH;
            case 8: return Interval.PERFECT_FIFTH;
            case 9: return Interval.PERFECT_FIFTH;
            case 10: return Interval.MAJOR_SIXTH;
            case 11: return Interval.MINOR_SIXTH;
            case 12: return Interval.MAJOR_SEVENTH;
            case 13: return Interval.MAJOR_SEVENTH;
            case 14: return Interval.MINOR_SEVENTH;
            case 15: return Interval.OCTAVE;
            case 16: return Interval.OCTAVE;
            case 17: return Interval.TRITONE;
            case 18: return Interval.MINOR_NINTH;
            case 19: return Interval.MAJOR_NINTH;
        }
        return Interval.ALL;
    }

    /**
     * Gets the user's progress on rhythm based on the user's rank.
     * This is used in generating appropriate melodies.
     * @return
     */
    public Rhythm getCurrentRhythm() {
        switch(rank) {
            case 0: return Rhythm.WHOLE_NOTE;
            case 1: return Rhythm.WHOLE_NOTE;
            case 2: return Rhythm.HALF_NOTE;
            case 3: return Rhythm.HALF_NOTE;
            case 4: return Rhythm.HALF_NOTE;
            case 5: return Rhythm.QUARTER_NOTE;
            case 6: return Rhythm.QUARTER_NOTE;
            case 7: return Rhythm.QUARTER_NOTE;
            case 8: return Rhythm.QUARTER_NOTE;
            case 9: return Rhythm.EIGHTH_NOTE;
            case 10: return Rhythm.EIGHTH_NOTE;
            case 11: return Rhythm.EIGHTH_NOTE;
            case 12: return Rhythm.EIGHTH_NOTE;
            case 13: return Rhythm.HALF_TRIPLES;
            case 14: return Rhythm.HALF_TRIPLES;
            case 15: return Rhythm.HALF_TRIPLES;
            case 16: return Rhythm.QUARTER_TRIPLES;
            case 17: return Rhythm.QUARTER_TRIPLES;
            case 18: return Rhythm.QUARTER_TRIPLES;
            case 19: return Rhythm.QUARTER_TRIPLES;
        }
        return Rhythm.ALL;
    }

    /**
     * The user's current number of passes at the current rank.
     * @return
     */
    public int getPasses() {
        return passes;
    }

    /**
     * The user's current number of failures at the current rank.
     * @return
     */
    public int getFailures() {
        return failures;
    }

    /**
     * The user's current rank.
     * @return
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets the number of passes to a particular value. Useful when loading data.
     * Usually, an increment method should be used.
     * @param passes
     */
    public void setPasses(int passes) {
        this.passes = passes;
    }

    /**
     * Sets the user's number of failures. Useful when loading data but normally
     * user the increment method.
     * @param failures
     */
    public void setFailures(int failures) {
        this.failures = failures;
    }

    /**
     * Get the number of bars in the melody.
     * @return
     */
    public int getBars() {
        return MIN_BARS;
    }

    /**
     * Increment the number of passes. If the user rating is greater than a threshold
     * we increment the rank. We subtract the log (base 2) of the number of failures from
     * the number of passes. If the resulting value is greater than a threshold then we
     * increment the rank.
     */
    public void incrementPasses() {
        passes++;
        int failLog = (int) Math.round(Math.log((double)failures) / Math.log(2.0));
        if (passes - failLog > RANK_UP_THRESHOLD) {
            rank++;
            resetFields();
        }
    }

    /**
     * We increment the number of failures. A failure will never trigger a change in
     * rank so we don't need to check for that.
     */
    public void incrementFailures() {
        failures++;
    }

    /**
     * Resets the passes and failures. This is useful after we have incremented the rank.
     */
    public void resetFields() {
        passes = 0;
        failures = 0;
    }

    /**
     * Sets the rank to a particular value. This is useful when we load data. Otherwise
     * we only update the rank when we increment the number of passes enough times.
     * @param r
     */
    public void setRank(int r) {
        this.rank = r;
        resetFields();
    }

    /**
     * Saves the user data to the local file on the app.
     */
    public void save() {
        UserStatsWriter.writeStats(this);
    }

    /**
     * Returns a string with the UserStats fields.
     * @return
     */
    public String toString() {
        return "rank=" + rank + " passes=" + passes + " failures=" + failures;
    }
}

