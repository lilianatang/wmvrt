package com.wmvrt.prototype.evaluator;

import com.wmvrt.prototype.Director;
import com.wmvrt.prototype.generator.Note;

import be.tarsos.dsp.io.TarsosDSPAudioFloatConverter;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.pitch.AMDF;
import be.tarsos.dsp.pitch.DynamicWavelet;
import be.tarsos.dsp.pitch.FFTPitch;
import be.tarsos.dsp.pitch.FastYin;
import be.tarsos.dsp.pitch.McLeodPitchMethod;
import be.tarsos.dsp.pitch.PitchDetector;
import be.tarsos.dsp.pitch.Yin;

import java.io.IOException;

/**
 * Only contains static methods.
 * Performs an audio analysis of a performance. It converts the performance
 * back into notes (pitch and time). Once in this format, we can evaluate
 * the performance.
 */
public class AudioAnalysis {


    /**
     * Given a data buffer of bytes and a sampling rate, turns the data into
     * an array of floats which can be used in the FFT to find pitches.
     * @param bytes
     * @param sampleRate
     * @return
     */
    private static float[] audioFile(byte[] bytes, int sampleRate) {
        float[] buffer = new float[bytes.length / 2];
        TarsosDSPAudioFormat audioFormat = new TarsosDSPAudioFormat((float) sampleRate,
                16, 1, true, false);
        TarsosDSPAudioFloatConverter converter = TarsosDSPAudioFloatConverter.getConverter(audioFormat);
        converter.toFloatArray(bytes, buffer);

        return buffer;
    }

    /**
     * Breaks a data buffer of floats into an array of arrays. Each subarray
     * has the samples gathered for 1/24th of a second of the performance. This
     * is the granularity we use when analyzing the performance.
     * @param buffer
     * @param sampleRate
     * @return
     */
    private static float[][] createSections(float[] buffer, int sampleRate) {
        float[][] sections;
        float seconds = buffer.length / (float) sampleRate;
        System.out.println("Seconds: " + seconds);
        int nrSamples = (int) (seconds * 24);
        int sampleLen = (int) (sampleRate / 24);
        sections = new float[nrSamples][sampleLen];
        int bufferCounter = 0;
        for (int i = 0; i < nrSamples; i++) {
            for (int j = 0; j < sampleLen; j++) {
                sections[i][j] = buffer[bufferCounter++];
            }
        }
        return sections;
    }

    /**
     * Given a buffer (in PCM16 format) and the sampling rate, determines
     * the pitches performed and the timings. Four different pitch
     * detection algorithms are applied.
     * @param fullBuffer
     * @param sampleRate
     */
    public static void detect(byte[] fullBuffer, int sampleRate) {
        // Load the audio data
        float[] buffer = audioFile(fullBuffer, sampleRate);
        System.out.println("Length: " + buffer.length);

        // Break the data up into samples, 24 samples per second
        float[][] sections = createSections(buffer, sampleRate);

        // Get the estimate for each sample
        int nrOfSections = sections.length;

        // Set up the arrays for the four different pitch detection algorithms.
        float mpm[] = new float[nrOfSections];
        float fastYin[] = new float[nrOfSections];
        float amdf[] = new float[nrOfSections];
        float yin[] = new float[nrOfSections];
        int bufferSize = (int) (sampleRate / 24);

        // Sets up the pitch detection algorithms.
        PitchDetector dMpm = new McLeodPitchMethod(sampleRate, bufferSize);
        PitchDetector dFastYin = new FastYin(sampleRate, bufferSize);
        PitchDetector dAmdf = new AMDF(sampleRate, bufferSize);
        PitchDetector dYin = new Yin(sampleRate, bufferSize);

        // Gets the pitch detections estimate for each section of the data buffer.
        for (int i = 0; i < nrOfSections; i++) {
            mpm[i] = dMpm.getPitch(sections[i]).getPitch();
            fastYin[i] = dFastYin.getPitch(sections[i]).getPitch();
            amdf[i] = dAmdf.getPitch(sections[i]).getPitch();
            yin[i] = dYin.getPitch(sections[i]).getPitch();
        }

        // Convert to a note value
        // Output the results
        for (int i = 0; i < nrOfSections; i++) {
            String output = "";
            output += i + ":\t";
            output += mpm[i] + "/" + Note.getPitchName(mpm[i]).getName() + "\t";
            output += fastYin[i] + "/" + Note.getPitchName(fastYin[i]).getName() + "\t";
            output += amdf[i] + "/" + Note.getPitchName(amdf[i]).getName() + "\t";
            output += yin[i] + "/" + Note.getPitchName(yin[i]).getName() + "\t";
            System.out.println(output);
        }

        // Updates the director with the results and triggers the evaluation
        // of the data.
        Director.getDirector().setNrOfSections(nrOfSections);
        Director.getDirector().setMpm(mpm);
        Director.getDirector().setFastYin(fastYin);
        Director.getDirector().setAmdf(amdf);
        Director.getDirector().setYin(yin);
        Director.getDirector().triggerEvaluation();

    }
}
