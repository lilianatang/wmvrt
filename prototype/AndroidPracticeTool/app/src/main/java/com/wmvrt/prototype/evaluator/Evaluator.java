package com.wmvrt.prototype.evaluator;

import com.wmvrt.prototype.Director;
import com.wmvrt.prototype.generator.Bar;
import com.wmvrt.prototype.generator.Melody;
import com.wmvrt.prototype.generator.Note;

/**
 * The Evaluator, given a melody and a recording, evaluates the performance.
 * This is a singleton.
 */
public class Evaluator {
    private static Evaluator _instance = null;

    /**
     * The number of beats to skip before evaluating.
     */
    public static final int EXTRACT_OFFSET = 10;

    /**
     * Keep the constructor private so that we can only create it using the
     * singleton's get method.
     */
    private Evaluator() {
    }

    /**
     * Gets the single instance of the Evaluator.
     * @return
     */
    public static Evaluator get() {
        if (_instance == null) {
            _instance = new Evaluator();
        }
        return _instance;
    }

    /**
     * Given a melody and recordings, determines the evaluation.
     * Before this is called, we expect that pitch detection has
     * been run on the performance.
     * @param melody
     * @param recordings
     * @return
     */
    public Evaluation performEvaluator(Melody melody, float[][] recordings) {
        Evaluation evaluation = new Evaluation();
        Bar[] mBars = melody.getBars();
        int melodyLen = mBars.length * Bar.TICKS_PER_BAR;
        float[][] extracts = createExtracts(recordings, melodyLen);
        int[] diffs = calculateDifferences(extracts, mBars);
        evaluation.setDiffs(diffs);
        return evaluation;
    }

    /**
     * Calculates the diffs for a performance based on the pitch detections
     * run on the performance. For each pitch detection algorithm, we calculate
     * the difference between the note detected and the actual note in the melody.
     * We sum up the diffs for each pitch detection algorithm and divide it by the
     * number of pitch detectors. This gives us an average diff.
     * This is the 'diff'.
     * @param extract
     * @param mBars
     * @return
     */
    private int[] calculateDifferences(float[][] extract, Bar[] mBars) {
        int melodyLen = extract[0].length;
        int[] diffs = new int[melodyLen];
        for (int i = 0; i < melodyLen; i++) diffs[i] = 0;
        for (int extractIndex = 0; extractIndex < Director.NUMBER_OF_ANALYSES; extractIndex++) {
            for (int i = 0; i < melodyLen; i++) {
                Note recordedPitch = Note.getPitchName(extract[extractIndex][i]);
                Note melodyPitch = mBars[i / Bar.TICKS_PER_BAR].getTicks()[i % Bar.TICKS_PER_BAR].getNote();
                int recOrd = recordedPitch.ordinal();
                int mOrd = melodyPitch.ordinal();
                //diffs[i] = Math.max(recOrd, mOrd) - Math.min(recOrd, mOrd);
                diffs[i] += Math.min(Math.abs(recOrd - mOrd), Math.abs(mOrd - recOrd));
            }
        }
        for (int i = 0; i < melodyLen; i++) diffs[i] = Math.round((float) diffs[i] / (float) Director.NUMBER_OF_ANALYSES);
        return diffs;
    }

    /**
     * Creates the extracts. The pitch detection algorithms often give 0.0 when
     * they cannot figure out the pitch. When this happens, we fill in the pitch with
     * the previously detected pitch. This sounds like a hack, but is a reasonable
     * approach since the pitch is unlikely to dramatically change very quickly.
     * @param recording
     * @param melodyLen
     * @return
     */
    private float[][] createExtracts(float[][] recording, int melodyLen) {
        float[][] extracts = new float[Director.NUMBER_OF_ANALYSES][melodyLen];
        for(int extractIndex = 0; extractIndex < Director.NUMBER_OF_ANALYSES; extractIndex++) {
            float current = (float) -1.0;
            for (int i = 0; i < EXTRACT_OFFSET; i++) {
                if (recording[extractIndex][i] > 0.0) {
                    current = recording[extractIndex][i];
                }
            }
            for (int i = 0; i < melodyLen; i++) {
                extracts[extractIndex][i] = recording[extractIndex][EXTRACT_OFFSET + i];
                if (extracts[extractIndex][i] < 0.0) {
                    extracts[extractIndex][i] = current;
                }
                else {
                    current = extracts[extractIndex][i];
                }
            }
        }
        return extracts;
    }

}

