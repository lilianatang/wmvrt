package com.wmvrt.prototype.generator;

/**
 * A Rhythm is a grouping of ticks. The enum is ordered by complexity and speed.
 */
public enum Rhythm {
    WHOLE_NOTE,
    HALF_NOTE,
    QUARTER_NOTE,
    EIGHTH_NOTE,
    HALF_TRIPLES,
    QUARTER_TRIPLES,
    ALL;

    /**
     * Gets the next rank of rhythm.
     * @return
     */
    public Rhythm getNext() {
        if (this == ALL) return ALL;
        int currentVal = this.ordinal();
        currentVal++;
        return Rhythm.values()[currentVal];
    }
}
