package com.wmvrt.prototype.generator;

/**
 * A Bar is a musical unit. We break the bar into 24 ticks. That means that every
 * bar will contain between 1 and 24 notes.
 */
public class Bar {
    public static final int TICKS_PER_BAR = 24;

    private Tick ticks[] = null;

    /**
     * Constructs a bar. Creates all 24 ticks for the bar.
     */
    public Bar() {
        ticks = new Tick[TICKS_PER_BAR];
        for (int i = 0; i < TICKS_PER_BAR; i++) {
            ticks[i] = new Tick();
        }
    }

    /**
     * Gets the Bar's ticks.
     * @return
     */
    public Tick[] getTicks() {
        return ticks;
    }

    /**
     * Gets the Bar in string format.
     * @return
     */
    public String toString() {
        String s = "";
        for (Tick t : ticks) {
            s = s + t.toString();
        }
        return s;
    }
}
