package com.wmvrt.prototype.evaluator;

/**
 * Contains the information about an evaluation of a performance.
 */
public class Evaluation {
    private String outputString = "";
    private int[] diffs = null;
    private float grade;

    public Evaluation() {
        outputString = "";
        grade = Float.MAX_VALUE;
    }

    /**
     * Sets the display text for this evaluation.
     * @param text
     */
    public void setOutputString(String text) {
        outputString = text;
    }

    /**
     * Gets the display string for the evaluation.
     * @return
     */
    public String toString() {
        return outputString;
    }

    /**
     * Gets the diffs - an array of integers.
     * @return
     */
    public int[] getDiffs() {
        return diffs;
    }

    /**
     * Sets the array of diffs and recalculates the grade based on the diffs.
     * @param diffs
     */
    public void setDiffs(int[] diffs) {
        this.diffs = diffs;
        calculateGrade();
    }

    /**
     * Calculates the grade based on the diffs.
     */
    private void calculateGrade() {
        float grade = (float) 0.0;
        double total = 0;
        for (int i = 0; i < diffs.length; i++) {
            total += (double) diffs[i];
        }
        grade = (float)total / (float) diffs.length;
        setGrade(grade);
    }

    /**
     * Gets the grade.
     * @return
     */
    public float getGrade() {
        return grade;
    }

    /**
     * Sets the grade and updates the output string.
     * @param grade
     */
    public void setGrade(float grade) {
        this.grade = grade;
        float lowest = Float.MAX_VALUE;
        int lowIndex = Integer.MAX_VALUE;

        StringBuffer b = new StringBuffer();

        b.append("");
        for (int i = 0; i < diffs.length; i++) {
            if (i > 0) b.append(' ');
            b.append(diffs[i]);
        }
        b.append('\n');
        b.append("Final Grade (lower is better): ");
        b.append(grade);
        b.append('\n');

        outputString = b.toString();
    }

    /**
     * Is the current grade a pass or fail?
     * @return
     */
    public boolean isPass() {
        return grade < 1.0;
    }

}

