package com.wmvrt.prototype.userstats;


import com.wmvrt.prototype.MainActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Reads the user stats file for the app.
 * If it does not exist, it creates an empty file to start with and then
 * reads it in.
 */
public class UserStatsReader {
    public static void readStats(UserStats userStats) {
        File file = new File(MainActivity.context.getFilesDir(),"mydir");
        if(!file.exists()){
            UserStatsWriter.writeStats(userStats);
        }

        try{
            File userStatsFile = new File(file, UserStatsWriter.USER_STATS_FILENAME);
            FileReader reader = new FileReader(userStatsFile);
            BufferedReader br = new BufferedReader(reader);
            String rankLine = br.readLine();
            String passLine = br.readLine();
            String failLine = br.readLine();
            userStats.setRank(convert(rankLine));
            userStats.setPasses(convert(passLine));
            userStats.setFailures(convert(failLine));

            reader.close();

        }catch (Exception e){
            e.printStackTrace();

        }
    }

    /**
     * Converts a string to a number.
     * @param line
     * @return
     */
    private static int convert(String line) {
        if (line == null) return 0;

        try {
            return Integer.parseInt(line.trim());
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            System.exit(1);
        }
        return 0;
    }
}

