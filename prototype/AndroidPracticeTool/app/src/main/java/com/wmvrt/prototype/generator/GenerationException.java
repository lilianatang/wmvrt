package com.wmvrt.prototype.generator;

/**
 * An exception that gets thrown when there's an error generating a melody.
 */
public class GenerationException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6662169406550518044L;

    public GenerationException(String msg) {
        super(msg);
    }
}
