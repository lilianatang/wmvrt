#include <jni.h>
#include <android/input.h>
#include "AudioEngine.h"

static AudioEngine *audioEngine = new AudioEngine();

extern "C" {

JNIEXPORT void JNICALL
Java_com_wmvrt_prototype_MainActivity_touchEvent(JNIEnv *env, jobject obj, jint action) {
    switch (action) {
        case AMOTION_EVENT_ACTION_DOWN:
            audioEngine->setToneOn(true);
            break;
        case AMOTION_EVENT_ACTION_UP:
            audioEngine->setToneOn(false);
            break;
        default:
            break;
    }
}

JNIEXPORT jint JNICALL
Java_com_wmvrt_prototype_MainActivity_startEngine(JNIEnv *env, jobject /* this */) {
    return audioEngine->start();
}

JNIEXPORT void JNICALL
Java_com_wmvrt_prototype_MainActivity_stopEngine(JNIEnv *env, jobject /* this */) {
    audioEngine->stop();
}

JNIEXPORT void JNICALL
Java_com_wmvrt_prototype_MainActivity_resetMelody(JNIEnv *env, jobject /* this */) {
    audioEngine->resetMelody();
}

JNIEXPORT void JNICALL
Java_com_wmvrt_prototype_MainActivity_addNote(JNIEnv *env, jobject obj, jdouble freq, jint time) {
    audioEngine->addNote(freq, time);
}

}
