//
// Created by joelb on 2019-11-02.
//

#ifndef WAVEMAKER_AUDIOENGINE_H
#define WAVEMAKER_AUDIOENGINE_H

#include <aaudio/AAudio.h>
#include "Oscillator.h"

/*
 * The audio engine is responsible for playing the melody.
 * When it is started, it uses the oscillator to generate
 * samples. It passes through to the oscillator the
 * methods necessary to set up melodies. This is so that
 * we only need to expose the AudioEngine class via JNI
 * to the Java code.
 */
class AudioEngine {
public:
    /*
     * Starts the sound playing. Does so by playing the
     * current melody set up in the oscillator.
     */
    int32_t start();

    /*
     * Stops the melody from playing.
     */
    void stop();

    /*
     * Restarts the audio engine.
     */
    void restart();

    /*
     * Indicates the the melody should be played.
     */
    void setToneOn(bool isToneOn);

    /*
     * Empties the melody buffer on the oscillator.
     */
    void resetMelody();

    /*
     * Adds a note to the melody buffer on the oscillator.
     * The note is specified by the frequency and the number
     * of frames to play the note.
     */
    void addNote(float frequency, int32_t time);

private:
    Oscillator oscillator_;
    AAudioStream *stream_;
};


#endif //WAVEMAKER_AUDIOENGINE_H
