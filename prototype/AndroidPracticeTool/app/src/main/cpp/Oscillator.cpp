//
// Created by joelb on 2019-11-02.
//

#include "Oscillator.h"
#include <math.h>

#define TWO_PI (3.14159 * 2)
#define AMPLITUDE 0.3
#define FREQUENCY 440.0
#define FRAME_LIMIT 30000

/**
 * Sets the sample rate, and also recalculates the phase increment based
 * on the sampling rate.
 * @param sampleRate
 */
void Oscillator::setSampleRate(int32_t sampleRate) {
    sampleRate_ = sampleRate;
    calculatePhaseIncrement();
}

/*
 * Calculates the phase increment. This is based on the current frequency and
 * the sample rate, so it must be recalculated any time either value changes.
 */
void Oscillator::calculatePhaseIncrement() {
    phaseIncrement_ = (TWO_PI * notes[currentNote_]) / (double) sampleRate_;
}

/*
 * Determines if the oscillator produces samples or not.
 */
void Oscillator::setWaveOn(bool isWaveOn) {
    isWaveOn_.store(isWaveOn);
}

/*
 * Fills a data buffer with the next samples in the oscillation
 * sequence.
 */
void Oscillator::render(float *audioData, int32_t numFrames) {

    if (!isWaveOn_.load()) phase_ = 0;

    for (int i = 0; i < numFrames; i++) {

        if (isWaveOn_.load()) {

            /*
             * When we change notes, we need to change the wave. We do this
             * by recalculating the phase increment. We know when we are at
             * the end of a note by the frame count for each note.
             * If we reach the end of the note sequence, we start over.
             */
            frameCount_++;
            if (frameCount_ > times[currentNote_]) {
                frameCount_ = 0;
                currentNote_++;
                if (currentNote_ >= nrOfNotes) currentNote_ = 0;
                calculatePhaseIncrement();
            }

            // Calculates the next sample value for the sequence.
            // We combine a full powered sine wave with a hint
            // of a square wave to create the sound.
            float sinPart = sin(phase_);
            if ((sinPart) > 0) sinPart += 0.2;
            else sinPart -= 0.2;
            audioData[i] = (float) (sinPart * AMPLITUDE);

            // Increments the phase, handling wrap around.
            phase_ += phaseIncrement_;
            if (phase_ > TWO_PI) phase_ -= TWO_PI;

        } else {
            // Outputs silence by setting sample value to zero.
            audioData[i] = 0;
        }
    }
}

/*
 * Resets the melody but resetting the counters for the melody.
 * We don't overwrite the notes in the note array since that
 * is done when new notes are added.
 */
void Oscillator::resetMelody() {
    nrOfNotes = 0;
    currentNote_ = 0;
}

/*
 * Add a note to the melody. The note is specified by the frequency
 * (pitch) of the note, and the number of frames it gets played.
 * We have a limit to the number of notes in a melody. If we
 * try to add more notes, we just replace the last note.
 */
void Oscillator::addNote(double frequency, int32_t time) {
    notes[nrOfNotes] = frequency;
    times[nrOfNotes] = time;
    nrOfNotes++;
    if (nrOfNotes >= NOTE_LIMIT) nrOfNotes--;
}