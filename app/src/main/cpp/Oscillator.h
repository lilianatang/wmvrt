//
// Created by joelb on 2019-11-02.
//

#ifndef WAVEMAKER_OSCILLATOR_H
#define WAVEMAKER_OSCILLATOR_H

#include <atomic>
#include <stdint.h>

// We can handle a melody of at most 100 notes.
#define NOTE_LIMIT 100

/*
 * The Oscillator class is responsible to storing a melody and
 * synthesizing the notes.
 */
class Oscillator {
public:
    // Starts the melody playing or stops it, depending on the isWaveOn value.
    void setWaveOn(bool isWaveOn);

    // Sets the sample rate. This is based on the phone. This is necessary
    // for generating the pitch appropriately.
    void setSampleRate(int32_t sampleRate);

    // This is a callback that fills a data array with samples.
    void render(float *audioData, int32_t numFrames);

    // The phase difference between every sample.
    void calculatePhaseIncrement();

    // Resets the melody to be zero notes long.
    void resetMelody();

    // Add a note with the indicated frequency and time
    // to the current melody.
    void addNote(double frequency, int32_t time);

private:
    std::atomic<bool> isWaveOn_{false};
    double phase_ = 0.0;
    double phaseIncrement_ = 0.0;
    double frequency_ = 440.0;
    int32_t sampleRate_ = 44000;
    int64_t frameCount_ = 0;
    double notes[NOTE_LIMIT];
    int32_t times[NOTE_LIMIT];
    int32_t nrOfNotes = 0;
    int32_t currentNote_ = 0;
};


#endif //WAVEMAKER_OSCILLATOR_H
