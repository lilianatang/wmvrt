#include <jni.h>
#include <android/input.h>
#include "AudioEngine.h"

static AudioEngine *audioEngine = new AudioEngine();

extern "C" {

JNIEXPORT void JNICALL
Java_com_matching_pitch_pitchmatching_PracticeActivity_touchEvent(JNIEnv *env, jobject obj, jint action) {
    switch (action) {
        case AMOTION_EVENT_ACTION_DOWN:
            audioEngine->setToneOn(true);
            break;
        case AMOTION_EVENT_ACTION_UP:
            audioEngine->setToneOn(false);
            break;
        default:
            break;
    }
}

JNIEXPORT jint JNICALL
Java_com_matching_pitch_pitchmatching_PracticeActivity_startEngine(JNIEnv *env, jobject /* this */) {
    return audioEngine->start();
}

JNIEXPORT void JNICALL
Java_com_matching_pitch_pitchmatching_PracticeActivity_stopEngine(JNIEnv *env, jobject /* this */) {
    audioEngine->stop();
}

JNIEXPORT void JNICALL
Java_com_matching_pitch_pitchmatching_PracticeActivity_resetMelody(JNIEnv *env, jobject /* this */) {
    audioEngine->resetMelody();
}

JNIEXPORT void JNICALL
Java_com_matching_pitch_pitchmatching_PracticeActivity_addNote(JNIEnv *env, jobject obj, jdouble freq, jint time) {
    audioEngine->addNote(freq, time);
}

}
