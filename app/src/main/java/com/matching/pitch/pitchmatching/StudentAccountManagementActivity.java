package com.matching.pitch.pitchmatching;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.net.Uri;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class StudentAccountManagementActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
        final Button startBtn = (Button) findViewById(R.id.sendEmail);
        startBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (v == startBtn){

                    Intent intent0 = new Intent(StudentAccountManagementActivity .this, RegisterActivity.class);
                    startActivity(intent0);

                }
            }
        });
    }
}
        /*startBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                sendEmail();
            }
        });
    }

// write code to switch activity here

    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[IMPORTANT] Account Creation");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Please follow this link to install the application. " +
                "Your username is your email." +
                "Your temporary password is: Vienna Boys Choir." +
                "For security purposes, please update your password after the first login");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
            finish();
            Log.i("Finished sending...", "");

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(AdminMainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT);
        }
    }
}*/
