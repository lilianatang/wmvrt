package com.matching.pitch.pitchmatching;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ProgressBar;
import androidx.appcompat.widget.Toolbar;
import java.util.*;

import android.widget.Toast;
import java.lang.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.matching.pitch.UserInformation;


public class MainActivity extends AppCompatActivity {
    private EditText inputEmail, inputPassword;
    //private FirebaseAuth auth;
    private ProgressBar progressBar;
    private Button btnLogin, btnReset;
    private DatabaseReference myRef;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String userID;
    private static final String TAG = "AdminMainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set the view now
        setContentView(R.layout.activity_login);
        // UI references
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnReset = (Button) findViewById(R.id.btn_reset_password);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Toast toast = Toast.makeText(getApplicationContext(), "Successfully signed in with: " + user.getEmail(), Toast.LENGTH_SHORT);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    Toast toast = Toast.makeText(getApplicationContext(), "Successfully signed out", Toast.LENGTH_SHORT);
                }
            }
        };
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ResetPasswordActivity.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString();
                final String password = inputPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please enter email address!", Toast.LENGTH_SHORT);
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please enter your password!", Toast.LENGTH_SHORT);
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                //authenticate user
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                        //If sign in fails, display a message to the user. If sign in succeeds
                                        // the auth state listener will be notified and logic to handle the signed in user can be handled in the listener.
                                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        inputPassword.setError("Please enter at least six characters!");
                                    } else {
                                        Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    // View database here - user is already signed in
                                    mAuth = FirebaseAuth.getInstance();
                                    mFirebaseDatabase = FirebaseDatabase.getInstance();
                                    myRef = mFirebaseDatabase.getReference("users");;
                                    final FirebaseUser user = mAuth.getCurrentUser();
                                    userID = user.getUid();
                                    Log.d(TAG, "showUserEmail: " + user.getEmail());
                                    myRef.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            // This method is called once with the initial value and again whenever data at this location is updated
                                            String role;
                                            ArrayList<UserInformation> array = new ArrayList<>();
                                            for (DataSnapshot childSnapshot: dataSnapshot.getChildren()){
                                                User user_db = new User(childSnapshot);
                                                Log.d(TAG, "email: " + user_db.getEmail());
                                                Log.d(TAG, "groups: " + user_db.getGroups());
                                                Log.d(TAG, "isAdmin? " + user_db.getGroups().get("admins"));
                                                Log.d(TAG, "EmailFromAuth: " + user.getEmail());
                                                if ((user.getEmail()).equals(user_db.getEmail())){

                                                    if ((Boolean)user_db.getGroups().get("admins") == true) {
                                                        Intent intent = new Intent(MainActivity.this, AdminMainActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }

                                                    else if ((Boolean)user_db.getGroups().get("teachers") == true) {
                                                        Intent intent = new Intent(MainActivity.this, TeacherMainActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }

                                                    else if ((Boolean)user_db.getGroups().get("students") == true) {
                                                        Intent intent = new Intent(MainActivity.this, StudentMainActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }

                                                    else {
                                                        Log.d(TAG, "Sorry, no role is assigned to this user");
                                                    }
                                                }
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                }
                            }

                        });


            }


        });
    }



}
