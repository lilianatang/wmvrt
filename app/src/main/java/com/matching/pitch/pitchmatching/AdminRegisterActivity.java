package com.matching.pitch.pitchmatching;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.lang.*;
import java.util.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.FirebaseAuthException;

public class AdminRegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mFirebaseDatabase= FirebaseDatabase.getInstance().getReference("users");
    private DatabaseReference mFirebaseDatabase_admins = FirebaseDatabase.getInstance().getReference("admins");
    private FirebaseDatabase mFirebaseInstance;
    private EditText password;
    private EditText email;
    private EditText name;
    private Button button_register;
    private Button button_send_email;
    private static final String TAG = "EmailPassword";
    private String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register);
        email = (EditText) findViewById(R.id.signup_email_input);
        password=(EditText) findViewById(R.id.signup_password_input);
        name=(EditText) findViewById(R.id.student_name);
        button_register = (Button)findViewById(R.id.button_register);
        button_send_email = (Button)findViewById(R.id.button_send_email);
        mAuth = FirebaseAuth.getInstance();
        //mFirebaseInstance = FirebaseDatabase.getInstance();


        // Save/ update the user

        button_register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (v == button_register) {
                    RegisterUser();
                }
            }
        });

        button_send_email.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (v == button_send_email) {
                    sendEmail();
                }
            }
        });
    }

    public void RegisterUser(){
        String Email= email.getText().toString().trim();
        String Password = password.getText().toString().trim();
        String Name = name.getText().toString().trim();
        //get reference to 'students' node
        //DatabaseReference mDatabase =  FirebaseDatabase.getInstance().getReference("admins");
        // Creating new user node, which returns the unique key value
        String userId = mFirebaseDatabase.push().getKey();
        // creating user object
        User user = new User(Name,Email,"admins");
        Admin admin = new Admin(Name, Email);
        mFirebaseDatabase.child(userId).setValue(user);
        mFirebaseDatabase_admins.child(userId).setValue(admin);
        if (TextUtils.isEmpty(Email)){
            Toast.makeText(this, "The email field is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(Password)) {
            Toast.makeText(this, "The password field is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.createUserWithEmailAndPassword(Email, Password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        try {
                            // check if successful
                            if (task.isSuccessful()) {
                                //sendEmail(Email, Password);
                                // User is successfully registered and logged in
                                // go back to main Activity here
                                Log.d(TAG, "createUserWithEmail:success");
                                //FirebaseUser user = mAuth.getCurrentUser();
                                Toast.makeText(AdminRegisterActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                finish();
                                startActivity(new Intent(getApplicationContext(), AdminMainActivity.class));
                            } else {
                                FirebaseAuthException e = (FirebaseAuthException)task.getException();
                                Toast.makeText(AdminRegisterActivity.this, "Could not register, try again please" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }


                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        //List provider = user.getProviderData().get(0);
                        List<? extends UserInfo> infos = user.getProviderData();
                        if (user != null) {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            Map<String, String> map = new HashMap<>();
                            for (UserInfo ui: infos){
                                map.put("Email", ui.getEmail());
                            }

                            /*if(user.getProviderData().containsKey("displayName")) {
                                map.put("displayName", user.getProviderData().get("displayName").toString());
                            }*/
                            ref.child("users").child(user.getUid()).setValue(map);
                        }
                    }


                });
    }

    protected void sendEmail() {
        String Email= email.getText().toString().trim();
        String Password = password.getText().toString().trim();
        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[IMPORTANT] PITCH MATCHING APP - ACCOUNT CREATION");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Please follow this link to install the application. " +
                "Your username is: " + Email + "\n" +
                "Your temporary password is: " + Password + "\n" +
                "For security purposes, please update your password after the first login");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
            finish();
            Log.i("Finished sending...", "");

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(AdminRegisterActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT);
        }
    }
}
