package com.matching.pitch.pitchmatching;

public class Teacher {
    public String name;
    public String email;
    public Teacher(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
