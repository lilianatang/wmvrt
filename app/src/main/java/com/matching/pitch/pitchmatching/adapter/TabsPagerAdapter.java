package com.matching.pitch.pitchmatching.adapter;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.matching.pitch.pitchmatching.AddStudentFragment;
import com.matching.pitch.pitchmatching.ViewStudentFragment;


public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new AddStudentFragment();
		case 1:
			// Games fragment activity
			return new ViewStudentFragment();
		//case 2:
			// Movies fragment activity
			//return new MoviesFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
