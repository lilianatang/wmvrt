package com.matching.pitch.pitchmatching;

public class Student {
    public String name;
    public String email;
    public Student(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
