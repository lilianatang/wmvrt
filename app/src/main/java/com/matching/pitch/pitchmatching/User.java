package com.matching.pitch.pitchmatching;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.*;
@IgnoreExtraProperties
public class User {

    public String name;
    public String email;
    public Map<String, Boolean> groups = new HashMap<String, Boolean>();

    public User(DataSnapshot snapshot){
        name = snapshot.child("name").getValue(String.class);
        email = snapshot.child("email").getValue(String.class);
        GenericTypeIndicator<Map<String, Boolean>> MapList = new GenericTypeIndicator<Map<String, Boolean>>() {};
        groups = snapshot.child("groups").getValue(MapList);
    }

    public User(String name, String email, String role) {
        this.name = name;
        this.email = email;
        if (role == "admins") {
            groups.put("admins", true);
            groups.put("teachers", false);
            groups.put("students", false);
        }
        else if (role ==  "teachers") {
            groups.put("admins", false);
            groups.put("teachers", true);
            groups.put("students", false);
        }
        else {
            groups.put("admins", false);
            groups.put("teachers", false);
            groups.put("students", true);
        }
    }

    public String getName(){
        return this.name;
    }

    public Map getGroups(){
        return this.groups;
    }

    public String getEmail(){
        return this.email;
    }

}
