package com.matching.pitch.pitchmatching;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.matching.pitch.practice.PracticeDirector;
import com.matching.pitch.practice.generator.Melody;
import com.matching.pitch.practice.generator.SequenceItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.Vector;

public class PracticeActivity extends AppCompatActivity {
    public static final int FULL_BUFFER_SIZE = 500000;
    public static PracticeActivity THIS;
    private byte[] fullBuffer = new byte[FULL_BUFFER_SIZE];
    private int sampleRate = 0;
    public static Context context = null;
    boolean isRecording = false;
    int bufferSize = 0;
    private Thread recordingThread = null;
    private int bufferIndex = 0;
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            THIS.stopPlayback();
        }
    };
    Runnable timerStopRecording = new Runnable() {
        @Override
        public void run() {
            THIS.stopRecording();
        }
    };
    AudioRecord recorder = null;

    static {
        System.loadLibrary("native-lib");
    }

    private native void touchEvent(int action);

    private native int startEngine();

    private native void stopEngine();

    private native void resetMelody();

    private native void addNote(double freq, int time);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        THIS = this;
        context = getApplicationContext();
        sampleRate = startEngine();
        System.out.println("Starting... sample rate = " + sampleRate);
        PracticeDirector.getDirector().setMainActivity(this);
        int bufferSize = AudioRecord.getMinBufferSize(sampleRate,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
                sampleRate,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

//            // Permission is not granted
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.RECORD_AUDIO)) {
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//            } else {
//                // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    99);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
        } else {
            // Permission has already been granted
        }
    }

    public void onPlayButtonAction(View view) {
        resetMelody();
        Vector<SequenceItem> n = PracticeDirector.getDirector().generateSequence(sampleRate);
        int duration = Melody.durationInMilliseconds(n, sampleRate);
        for (SequenceItem si : n) {
            System.out.println("Adding " + si.freq + "/" + si.time);
            addNote(si.freq, si.time);
        }
        System.out.println("Notes added to synth");
        touchEvent(MotionEvent.ACTION_DOWN);
        System.out.println("DELAY ADDED: " + duration + " : " + System.currentTimeMillis());
        timerHandler.postDelayed(timerRunnable, duration);
        setText("Listen to the melody");
    }

    public void stopPlayback() {
        System.out.println("STOPPING PLAYBACK NOW! " + System.currentTimeMillis());
        touchEvent(MotionEvent.ACTION_UP);
        startRecording();
        setText("Sing back the melody");
    }

    @Override
    public void onDestroy() {
        stopEngine();
        super.onDestroy();
    }

    private void startRecording() {
        timerHandler.postDelayed(timerStopRecording, 5500);
        recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
                sampleRate,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                BufferElements2Rec * BytesPerElement);
        recorder.startRecording();
        isRecording = true;
        bufferIndex = 0;
        recordingThread = new Thread(new Runnable() {
            public void run() {
                writeAudioDataToFile();
            }
        }, "AudioRecorder Thread");
        System.out.println("STARTING RECORDING");
        recordingThread.start();
    }

    int BufferElements2Rec = 2048; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format

    //convert short to byte
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

    private void writeAudioDataToFile() {
        // Write the output audio in byte

        short sData[] = new short[BufferElements2Rec];

        while (isRecording) {
            // gets the voice output from microphone to byte format

            recorder.read(sData, 0, BufferElements2Rec);
            // // writes the data to full buffer from buffer
            // // stores the voice buffer
            byte bData[] = short2byte(sData);
            for (int i = 0; i < bData.length; i++) {
                fullBuffer[bufferIndex++] = bData[i];
                if (bufferIndex >= FULL_BUFFER_SIZE) {
                    System.out.println("BUFFER OVERFLOW");
                    bufferIndex--;
                }
            }
        }
    }

    private void stopRecording() {
        setText("Analyzing your performance. This may take some time...");
        recorder.stop();
        // stops the recording activity
        if (recorder != null) {
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
        }
        PracticeDirector.getDirector().processRecording(fullBuffer, sampleRate);
    }

    public void setText(String value) {
        System.out.println("Setting text to: " + value);
        TextView myText = (TextView) findViewById(R.id.textView);
        myText.setText(value);
        myText.invalidate();
        myText.requestLayout();
    }
}
