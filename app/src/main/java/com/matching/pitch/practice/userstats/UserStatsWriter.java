package com.matching.pitch.practice.userstats;

import com.matching.pitch.practice.PracticeDirector;

import java.io.File;
import java.io.FileWriter;

/**
 * Takes care of writing out the user stats to a file.
 */
public class UserStatsWriter {
    public static final String USER_STATS_FILENAME = "userstats.txt";

    /**
     * Writes out the user stats. The file gets stored locally on the
     * app in the app's directory space as the file mydir/userstats.txt
     * @param userStats
     */
    public static void writeStats(UserStats userStats) {
        File file = new File(PracticeDirector.getDirector().getMainActivity().context.getFilesDir(),"mydir");
        if(!file.exists()){
            file.mkdir();
        }

        try{
            File userStatsFile = new File(file, USER_STATS_FILENAME);
            FileWriter writer = new FileWriter(userStatsFile);
            String fileContents = "" + userStats.getRank() + "\n" + userStats.getPasses() + "\n" + userStats.getFailures() + "\n";
            writer.append(fileContents);
            writer.flush();
            writer.close();

        }catch (Exception e){
            e.printStackTrace();

        }

    }
}
