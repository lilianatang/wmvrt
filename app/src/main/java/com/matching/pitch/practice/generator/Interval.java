package com.matching.pitch.practice.generator;

/**
 * Intervals are differences in pitches.
 */
public enum Interval {
    TONIC,
    MAJOR_SECOND,
    MINOR_SECOND,
    MAJOR_THIRD,
    MINOR_THIRD,
    PERFECT_FOURTH,
    PERFECT_FIFTH,
    MAJOR_SIXTH,
    MINOR_SIXTH,
    MAJOR_SEVENTH,
    MINOR_SEVENTH,
    OCTAVE,
    TRITONE,
    MINOR_NINTH,
    MAJOR_NINTH,
    MINOR_TENTH,
    MAJOR_TENTH,
    MINOR_ELEVENTH,
    MAJOR_ELEVENTH,
    MINOR_TWELFTH,
    MAJOR_TWELFTH,
    ALL;

    /**
     * Gets the next interval based on difficulty.
     * @return
     */
    public Interval getNext() {
        if (this == ALL) return ALL;
        int currentVal = this.ordinal();
        currentVal++;
        return Interval.values()[currentVal];
    }

    /**
     * Returns the interval's ordinal value.
     * @return
     */
    public int asInt() {
        switch (this) {
            case TONIC: return 0;
            case MINOR_SECOND: return 1;
            case MAJOR_SECOND: return 2;
            case MINOR_THIRD: return 3;
            case MAJOR_THIRD: return 4;
            case PERFECT_FOURTH: return 5;
            case TRITONE: return 6;
            case PERFECT_FIFTH: return 7;
            case MINOR_SIXTH: return 8;
            case MAJOR_SIXTH: return 9;
            case MINOR_SEVENTH: return 10;
            case MAJOR_SEVENTH: return 11;
            case OCTAVE: return 12;
            case MINOR_NINTH: return 13;
            case MAJOR_NINTH: return 14;
            case MINOR_TENTH: return 15;
            case MAJOR_TENTH: return 16;
            case MINOR_ELEVENTH: return 17;
            case MAJOR_ELEVENTH: return 18;
            case MINOR_TWELFTH: return 19;
            case MAJOR_TWELFTH: return 20;
            default: return 0;
        }
    }
}

