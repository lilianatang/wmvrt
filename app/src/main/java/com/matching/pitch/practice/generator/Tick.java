package com.matching.pitch.practice.generator;

/**
 * A data structure for storing information about a Tick.
 */
public class Tick {
    public static final int MIN_OCTAVE = 0;
    public static final int MAX_OCTAVE = 6;
    private boolean isStart = false;
    private boolean isEnd = false;
    private Note note = null;
    private int octave = 3;

    public Tick() {}

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean isStart) {
        this.isStart = isStart;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean isEnd) {
        this.isEnd = isEnd;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public int getOctave() {
        return octave;
    }

    public void setOctave(int octave) {
        if (octave >= MIN_OCTAVE && octave <= MAX_OCTAVE) {
            this.octave = octave;
        }
    }

    public String toString() {
        String s = "";
        if (this.getNote() != null) {
            if (this.isStart) {
                s = s + this.getNote().toString();
            } else {
                s = s + "- ";
            }
        } else {
            s = s + ". ";
        }
        return s;
    }
}

