package com.matching.pitch.practice.generator;

/**
 * Creates a melody.
 * Assumes that the rhythm has already been figured out, and just adds appropriate notes in the
 * slots.
 */
public class MelodyGenerator {

    /**
     * Generates a melody given a Melody structure with the rhythm filled in, and the user's
     * current Interval rank.
     * @param melody
     * @param interval
     */
    public static void generate(Melody melody, Interval interval) {
        Note currentNote = Note.C;
        int currentOctave = 3;
        int relativeInterval = 0;
        for (Bar bar : melody.getBars()) {
            for (Tick tick : bar.getTicks()) {
                if (tick.isStart()) {
                    relativeInterval = addRandomNote(tick, currentNote, currentOctave, interval, relativeInterval);
                    currentNote = tick.getNote();
                    currentOctave = tick.getOctave();
                } else {
                    tick.setNote(currentNote);
                    tick.setOctave(currentOctave);
                }
            }
        }

    }

    /**
     * Adds a random note to a melody. It chooses a random interval and a random direction and
     * adds the appropriate note.
     * TODO: Add some more checks to make sure we don't wander too far out of range.
     * @param tick
     * @param currentNote
     * @param currentOctave
     * @param interval
     * @param relativeInterval
     * @return
     */
    private static int addRandomNote(Tick tick, Note currentNote, int currentOctave, Interval interval, int relativeInterval) {
        int randomVal = (int)(Math.random() * interval.ordinal());
        Interval randomInterval = Interval.values()[randomVal];
        int direction = 1;
        int randomDirection = (int)(Math.random() * 2);
        if (randomDirection == 0) direction = -1;

        int noteInt = currentNote.ordinal();
        int intervalInt = randomInterval.asInt() * direction;

        if (Math.abs(relativeInterval + intervalInt) > 16) {
            intervalInt = intervalInt * -1;
        }

        noteInt += intervalInt;
        currentOctave += (int) (noteInt / 12);
        noteInt = Math.abs(noteInt % 12);
        tick.setNote(Note.values()[noteInt]);
        tick.setOctave(currentOctave);
        return relativeInterval + intervalInt;
    }

}
