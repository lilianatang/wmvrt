package com.matching.pitch.practice.generator;

/**
 * A pitch. We give an enumeration over the note names (using sharps instead of flats).
 * We also have routines to convert to and from frequencies based on the octave.
 */
public enum Note {
    C,
    Cs,
    D,
    Ds,
    E,
    F,
    Fs,
    G,
    Gs,
    A,
    As,
    B;

    /**
     * Each row corresponds to the ordinal value of the enum, and the column refers to the
     * octave.
     */
    public static final double FREQS[][] = {
            {16.35,	32.70,	65.41,	130.81,	261.63,	523.25,	1046.50,	2093.00,	4186.01},
            {17.32,	34.65,	69.30,	138.59,	277.18,	554.37,	1108.73,	2217.46,	4434.92},
            {18.35,	36.71,	73.42,	146.83,	293.66,	587.33,	1174.66,	2349.32,	4698.64},
            {19.45,	38.89,	77.78,	155.56,	311.13,	622.25,	1244.51,	2489.02,	4978.03},
            {20.60,	41.20,	82.41,	164.81,	329.63,	659.26,	1318.51,	2637.02,	5274.04},
            {21.83,	43.65,	87.31,	174.61,	349.23,	698.46,	1396.91,	2793.83,	5587.65},
            {23.12,	46.25,	92.50,	185.00,	369.99,	739.99,	1479.98,	2959.96,	5919.91},
            {24.50,	49.00,	98.00,	196.00,	392.00,	783.99,	1567.99,	3135.96,	6271.93},
            {25.96,	51.91,	103.83,	207.65,	415.30,	830.61,	1661.22,	3322.44,	6644.88},
            {27.50,	55.00,	110.00,	220.00,	440.00,	880.00,	1760.00,	3520.00,	7040.00},
            {29.14,	58.27,	116.54,	233.08,	466.16,	932.33,	1864.66,	3729.31,	7458.62},
            {30.87,	61.74,	123.47,	246.94,	493.88,	987.77,	1975.53,	3951.07,	7902.13}
    };

    /**
     * Given a frequency, we determine the pitch name.
     * @param freq
     * @return
     */
    public static Note getPitchName(float freq)
    {
        if (freq < 1.0)
        {
            return C;
        }

        float val = freq;
        while (val < 247.0)
        {
            val = val * 2;
        }
        while (val > 523.0)
        {
            val = val / 2;
        }

        if (val < 270.0) return C;
        if (val < 285.0) return Cs;
        if (val < 306.0) return D;
        if (val < 320.0) return Ds;
        if (val < 339.0) return E;
        if (val < 359.0) return F;
        if (val < 380.0) return Fs;
        if (val < 404.0) return G;
        if (val < 432.0) return Gs;
        if (val < 453.0) return A;
        if (val < 485.0) return As;
        return B;
    }

    /**
     * Gets the note in string form.
     * @return
     */
    public String getName()
    {
        if (this == C) return "C ";
        if (this == Cs) return "C#";
        if (this == D) return "D ";
        if (this == Ds) return "D#";
        if (this == E) return "E ";
        if (this == F) return "F ";
        if (this == Fs) return "F#";
        if (this == G) return "G ";
        if (this == Gs) return "G#";
        if (this == A) return "A ";
        if (this == As) return "A#";
        return "B ";
    }

    /**
     * Given an octave, gives the frequency value for a Note.
     * @param octave
     * @return
     */
    public double getFrequency(int octave) {
        return FREQS[this.ordinal()][octave];
    }
}

