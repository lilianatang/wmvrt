package com.matching.pitch.practice.generator;

/**
 * Data structure used to convert melodies into notes suitable for the Audio Engine.
 */
public class SequenceItem {
    public double freq;
    public int time;
    public SequenceItem(double freq, int time) {
        this.freq = freq;
        this.time = time;
    }
}
