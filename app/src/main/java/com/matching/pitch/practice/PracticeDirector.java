package com.matching.pitch.practice;

import com.matching.pitch.pitchmatching.PracticeActivity;
import com.matching.pitch.practice.evaluator.AudioAnalysis;
import com.matching.pitch.practice.evaluator.Evaluation;
import com.matching.pitch.practice.evaluator.Evaluator;
import com.matching.pitch.practice.generator.GenerationException;
import com.matching.pitch.practice.generator.Generator;
import com.matching.pitch.practice.generator.Melody;
import com.matching.pitch.practice.generator.SequenceItem;
import com.matching.pitch.practice.userstats.UserStats;

import java.util.Vector;

public class PracticeDirector {
    private static PracticeDirector __instance = null;
    private UserStats userStats = null;
    private PracticeActivity mainActivity = null;
    private Melody melody = null;
    private int nrOfSections = 0;
    public static final int MPM = 0;
    public static final int FAST_YIN = 1;
    public static final int AMDF = 2;
    public static final int YIN = 3;
    public static final int NUMBER_OF_ANALYSES = 4;
    private float[][] analyses = null;
    private Evaluation evaluation = null;


    private PracticeDirector() {
        analyses = new float[NUMBER_OF_ANALYSES][];
        for (int i = 0; i < NUMBER_OF_ANALYSES; i++) analyses[i] = null;
    }

    public static PracticeDirector getDirector() {
        if (__instance == null) {
            __instance = new PracticeDirector();
        }
        return __instance;
    }

    public UserStats getUserStats() {
        if (userStats == null) {
            userStats = new UserStats();
        }
        return userStats;
    }

    public Vector<SequenceItem> generateSequence(int sampleRate) {
        melody = null;
        try {
            melody = Generator.generate(getUserStats());
        } catch(GenerationException ge) {
            ge.printStackTrace();
        }
        return melody.toSequence(sampleRate);
    }

    public void setMainActivity(PracticeActivity activity) {
        this.mainActivity = activity;
    }

    public PracticeActivity getMainActivity() {
        return mainActivity;
    }

    public void processRecording(byte[] buffer, int sampleRate) {
        System.out.println("BUFFER Has " + buffer.length + " bytes to process");
        AudioAnalysis.detect(buffer, sampleRate);
    }

    public void setNrOfSections(int nrOfSections) {
        this.nrOfSections = nrOfSections;
    }
    public int getNrOfSections() {
        return nrOfSections;
    }

    public float[] getMpm() {
        return analyses[MPM];
    }
    public void setMpm(float[] mpm) {
        this.analyses[MPM] = mpm;
    }
    public float[] getFastYin() {
        return analyses[FAST_YIN];
    }
    public void setFastYin(float[] fastYin) {
        this.analyses[FAST_YIN] = fastYin;
    }
    public float[] getAmdf() {
        return analyses[AMDF];
    }
    public void setAmdf(float[] amdf) {
        this.analyses[AMDF] = amdf;
    }
    public float[] getYin() {
        return analyses[YIN];
    }
    public void setYin(float[] yin) {
        this.analyses[YIN] = yin;
    }

    public void triggerEvaluation() {
        System.out.println("TRIGGER EVALUATION");
        setText("Evaluating (this may take a while)");
        // Compare melody to recordings.
        evaluation = Evaluator.get().performEvaluator(melody, analyses);
        if (evaluation.isPass()) {
            getUserStats().incrementPasses();
        } else {
            getUserStats().incrementFailures();
        }
        getUserStats().save();
        // Output the results to the evaluation panel.
        String text = getUserStats().toString() + "\n" + melody.toString() + "\n" + evaluation.toString();
        System.out.println(text);
        setText(text);
        //getEvaluationPanel().updateText(text);
//        if (debug) {
//            launchDebuggerDialog();
//        }
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setText(String text) {
        if (mainActivity != null) {
            mainActivity.setText(text);
        }
    }
}
